In this Section, we discuss the relationship between the coupling constants
and the cross section of a process involving the \Hboson.  We denote the coupling
constants as $a_n$, which could stand for $g_n$, $c_n$, or $\kappa_n$
as used in Section~\ref{sect:cp_couplings}.
The cross section of a process $i\to H\to f$ can be expressed as
%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
\frac{\mathrm{d}\sigma(i\to H\to f)}{\mathrm{d}s}\propto
\frac{\left(\sum  \alpha_{jk}^{(i)}a_ja_k\right)\left(\sum \alpha_{lm}^{(f)}a_la_m\right)}{(s-M_H^2)^2 + M_H^2\Gamma_{\rm tot}^2} \,.
\label{eq:diff-cross-section1}
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%%
where $\left(\sum  \alpha_{jk}^{(i)}a_ja_k\right)$ describes the production for a particular initial state $i$
and $\left(\sum  \alpha_{lm}^{(f)}a_la_m\right)$ describes the decay for a particular final state $f$.
Here we assume real coupling constants $a_n$, though these formulas can also be extended to complex couplings. 
The coefficients $\alpha_{jk}^{(i)}$ and $\alpha_{lm}^{(f)}$ evolve with $s$ and may be functions of kinematic observables.
These coefficients can be obtained from simulation, as we discuss in Section~\ref{sect:cp_mc}.
In this Section, we discuss integrated cross sections, and for this reason we deal with 
$\alpha_{jk}^{(i)}$ and $\alpha_{lm}^{(f)}$ as constants that have already been integrated over the kinematics. 
We will come back to the kinematic dependence in Section~\ref{sect:exp_kinematics}.

In the narrow-width approximation for \onshell\ production, we integrate Eq.~(\ref{eq:diff-cross-section1}) over $s$ in the relevant 
range, $\sim M_H\Gamma_{\rm tot}$ around the central value of $M_H^2$, to obtain the cross section for the process of interest
%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
\sigma(i\to H\to f)\propto
\frac{\left(\sum  \alpha_{jk}^{(i)}a_ja_k\right)\left(\sum \alpha_{lm}^{(f)}a_la_m\right)}{\Gamma_{\rm tot}} \,.
\label{eq:diff-cross-section2}
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%%
One can express the total width as
%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
\Gamma_{\rm tot}= \Gamma_{\rm known} + \Gamma_{\rm other} \,,
\label{eq:width_other}
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%%
where $\Gamma_{\rm known}$ represents decays to known particles and $\Gamma_{\rm other}$ represents
other unknown final states, either invisible or undetected in experiment. 

Without direct constraints on $\Gamma_{\rm other}$, if results are to be interpreted in terms of couplings via
the narrow-width approximation in Eq.~(\ref{eq:diff-cross-section2}), assumptions must be made on $\Gamma_\text{other}$.
However, in the case of the $ZZ$ and $WW$ final states, there is an interplay between the massive vector boson 
or the \Hboson\ going \offshell,  resulting in a sizable \offshell\ $H^*$ production~\cite{Kauer:2012hd}
with $(s-M_H^2)\gg M_H\Gamma_{\rm tot}$ in Eq.~(\ref{eq:diff-cross-section1}).
The resulting cross section in this region $s>(2M_W)^2$ is independent of the width. 
%%%%%%%%%%%%%%%%%%%%%
%\begin{eqnarray}
%\sigma(i\to H^* \to f)\propto \left(\sum  \alpha_{jk}^{(i)}a_ja_k\right)\left(\sum \alpha_{lm}^{(f)}a_la_m\right) \,,
%\label{eq:diff-cross-section3}
%\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%%
% where $\alpha_{jk}^{(i)}$ and $\alpha_{lm}^{(f)}$ differ from those in Eq.~(\ref{eq:diff-cross-section2}) due to the $s$ evolution. 
It should be noted that Eq.~(\ref{eq:diff-cross-section1}) represents only the signal part of the \offshell\ process 
with the \Hboson\ propagator. 
The full process involves background and its interference with the signal~\cite{Kauer:2012hd,Caola:2013yja}, 
as we illustrate in Section~\ref{sect:exp_offshell}.
%Nonetheless, Eq.~(\ref{eq:diff-cross-section3}) is sufficient to illustrate the idea. 
%The relative sizes of the cross sections in Eqs.~(\ref{eq:diff-cross-section2}) and (\ref{eq:diff-cross-section3}) form the basis
%for the \Hboson total width $\Gamma_{\rm tot}$ measurement~\cite{Caola:2013yja}, provided that the evolution 
%of Eq.~(\ref{eq:diff-cross-section1}) with $s$ is known. 
Nonetheless, the lack of width dependence in the \offshell\ region is the basis for the measurement of the \Hboson's
total width $\Gamma_{\rm tot}$~\cite{Caola:2013yja}, provided that the evolution of Eq.~(\ref{eq:diff-cross-section1}) with $s$ is known. 
Therefore, a joint analysis of the \onshell\ and \offshell\ regions provides a simultaneous measurement 
of $\Gamma_{\rm tot}$ and of the cross sections corresponding to each coupling $a_n$ in a process $i\to H^{(*)}\to f$, 
as illustrated in Refs.~\cite{Khachatryan:2015mma,Sirunyan:2019twz}.
In a combination of multiple processes, the measurement can be further interpreted
as constraints on $\Gamma_{\rm other}$ and the couplings, following Eqs.~(\ref{eq:diff-cross-section2}) and (\ref{eq:width_other}),
and with the help of the identity
%%%%%%%%%%%%%%%%%%%%%q
\begin{eqnarray}
{\Gamma_{\rm known}} = \sum_{f} \Gamma_f 
  = {\Gamma_{\rm tot}^{\rm SM}} \times
\sum_{f} \left( \frac{\Gamma_f^{\rm SM}}{\Gamma_{\rm tot}^{\rm SM}} \times \frac{\Gamma_f }{ \Gamma_f^{\rm SM}} \right)
= \sum_{f}\Gamma_f^{\rm SM}\sum_{lm}\alpha_{lm}^{(f)}a_la_m 
\,.
\label{eq:width}
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%%
The coefficients $\alpha_{lm}^{(f)}$ describe couplings to the known states and are normalized 
in such a way that $R_{f}(a_n)=\left(\sum  \alpha_{lm}^{(f)}a_la_m\right)=1$ in the SM,
and otherwise  $R_{f}(a_n)=\Gamma_f/\Gamma_f^{\rm SM}$.

In the following, we proceed to discuss the \onshell\ part of the measurements using the narrow-width approximation. 
In Table~\ref{tab:cformalism}, we summarize all the coefficients and functions $R_{f}$ needed to calculate 
${\Gamma_{\rm known}}$ in Eq.~(\ref{eq:width}).
These expressions with explicit coefficients $\alpha_{lm}^{(f)}$ help us to illustrate the relationship between the coupling 
constants introduced in Section~\ref{sect:cp_couplings} and experimental cross-section measurements. 
We will also use these expressions in Section~\ref{sect:exp_onshell} in application to particular measurements. 
For almost all calculations, we use the JHUGen framework implementation discussed in Section~\ref{sect:cp_mc}.
The only exceptions are $R_{\gamma\gamma}$ and $R_{Z\gamma}$, which are calculated using HDECAY~\cite{Djouadi:2018xqq,Fontes:2017zfn}.
The calculations are performed at LO in QCD and EW, with the $\overline{\mathrm{MS}}$-mass for the 
top quark $m_{t} =162.7$\,GeV and the \onshell\ mass for the bottom quark $m_{b} =4.18$\,GeV, and QCD scale $\mu = M_H/2$.

For all fermion final states $H\to q\bar{q}$, where we generically use $q=b,c,\tau,\mu$ to denote either quarks or leptons,
in the limit $m_q\ll M_H$ we obtain
%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
\label{eq:ratio-1}
R_{qq} = \kappa_q^2+\tilde\kappa_q^2
\,.
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%%

%============
\begin{table}[t]
\begin{center}
\captionsetup{justification=centerlast}
\caption{
Partial widths $\Gamma_f$ of the dominant $H\to f$ decay modes in the SM in the narrow-width approximation~\cite{deFlorian:2016spz} 
and their modifications with anomalous couplings at $M_H=125$\,GeV, where $\Gamma_{\rm tot}^{\rm SM}=4.088\times 10^{-3}$\,GeV. 
Final states with $\Gamma_f^{\rm SM}<\Gamma_{\mu\mu}^{\rm SM}$  are neglected. 
\label{tab:cformalism}
}
\begin{tabular}{cccc}
\hline\hline
\vspace{-0.3cm} \\
$H\to f$ channel &    $\Gamma_f^{\rm SM}/\Gamma_{\rm tot}^{\rm SM}$        &      $\Gamma_f / \Gamma_f^{\rm SM}$   & Eq. \\
\vspace{-0.3cm} \\
\hline
\vspace{-0.3cm} \\
$H\to b\bar{b}$ &  0.5824 & $(\kappa_b^2+\tilde\kappa_b^2)$  & Eq.~(\ref{eq:ratio-1}) \\
$H\to W^+W^-$ &  0.2137 & $R_{WW}(a_n)$  & Eq.~(\ref{eq:ratio-2}) \\
$H\to gg$ &  0.08187 & $R_{gg}(a_n)$  & Eq.~(\ref{eq:ratio-4}) \\
$H\to \tau^+\tau^-$ &  0.06272 & $(\kappa_\tau^2+\tilde\kappa_\tau^2)$  & Eq.~(\ref{eq:ratio-1}) \\
$H\to c\bar{c}$ &  0.02891 & $(\kappa_c^2+\tilde\kappa_c^2)$  & Eq.~(\ref{eq:ratio-1}) \\
$H\to ZZ/Z\gamma^*/\gamma^*\gamma^*$ &  0.02619 & $R_{ZZ/Z\gamma^*/\gamma^*\gamma^*}(a_n)$  & Eq.~(\ref{eq:ratio-3}) \\
$H\to \gamma\gamma$ &  0.002270 & $R_{\gamma\gamma}(a_n)$  & Eq.~(\ref{eq:ratio-5}) \\
$H\to Z\gamma$ &  0.001533 & $R_{Z\gamma}(a_n)$  & Eq.~(\ref{eq:ratio-6}) \\
$H\to\mu^+\mu^-$ &  0.0002176 & $(\kappa_\mu^2+\tilde\kappa_\mu^2)$  & Eq.~(\ref{eq:ratio-1}) \\
\vspace{-0.3cm} \\
\hline\hline
\end{tabular}
\end{center}
\end{table}
%============

For the gluon final state $H\to gg$, we allow for top and bottom quark contributions through the couplings from Eq.~(\ref{eq:Hffcoupl}).
In addition, we introduce a new heavy quark $Q$ with mass $m_Q\gg M_H$ and couplings to the \Hboson $\kappa_Q$ and $\tilde\kappa_Q$.
The result is
%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
\label{eq:ratio-4}
R_{gg} = &&
1.1068\, \kappa_t^2 + 0.0082\, \kappa_b^2 - 0.1150\, \kappa_t\kappa_b
	+ 2.5717\, \tilde\kappa_t^2 + 0.0091\, \tilde\kappa_b^2 - 0.1982\, \tilde\kappa_t\tilde\kappa_b \\
&&	+\, 1.0298\, \kappa_Q^{2} - 1.2095\, \kappa_Q \kappa_t - 0.1109\, \kappa_Q \kappa_b
	+ 2.3170\, \tilde\kappa_Q^{2} + 4.8821\, \tilde\kappa_Q \tilde\kappa_t - 0.1880\, \tilde\kappa_Q \tilde\kappa_b
	\,.
	\nonumber 
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%%
The $\kappa_Q$ and $\tilde\kappa_Q$ couplings are connected to the $g_2^{gg}$ and $g_4^{gg}$ point-like 
interactions introduced in Eq.~(\ref{eq:HVV}) through
%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
\label{eq:g2gg}
g_{2}^{gg} =-\alpha_s \kappa_Q/(6\pi)
\,,~~~~~~~~~
g_{4}^{gg} =-\alpha_s\tilde\kappa_Q/(4\pi)
\,
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%%
in the limit where $m_Q\gg M_H$.
The function $R_{gg}$ also describes the scaling of the gluon fusion cross section with anomalous coupling contributions. 
 Setting $\kappa_q=\kappa_t=\kappa_b$ and $\tilde\kappa_q=\tilde\kappa_t=\tilde\kappa_b$, we find the ratio 
$\sigma(\tilde\kappa_q=1)/\sigma(\kappa_q=1)=2.38$, which differs from the ratio for a very heavy quark
$\sigma(\tilde\kappa_Q=1)/\sigma(\kappa_Q=1)=(3/2)^2=2.25$ due to finite quark mass effects. 
The latter ratio follows from the observation $\sigma(g_4^{gg}=1)=\sigma(g_2^{gg}=1)$.
In experiment, it is hard to distinguish the point-like interactions $g_2^{gg}$ and $g_4^{gg}$, or equivalently $\kappa_Q$ 
and $\tilde\kappa_Q$, from the SM-fermion loops. 
In the $H\to gg$ decay, there is no kinematic difference. In the gluon fusion production, there are effects in the tails of 
distributions, such as the transverse momentum, or in the \offshell\ region, as we discuss in Section~\ref{sect:exp_offshell}.
However, in Section~\ref{sect:exp_onshell} these effects are negligible and we do not distinguish
the $g_2^{gg}$ and $g_4^{gg}$ couplings from the SM-fermion loops. 

For the $H\to WW\rightarrow$\,four-fermion final state, we set $\Lambda_1^{WW}=100$\,GeV in Eq.~(\ref{eq:HVV}) 
in order to keep all numerical coefficients of similar order, and rely on the $\kappa_1^{WW}=\kappa_2^{WW}$ relationship to obtain 
%%%%%%%%%%%%%%%%%%%%%
%\begin{equation}
\begin{eqnarray}
\label{eq:ratio-2}
R_{WW} = &&  \left(\frac{g_1^{WW}}{2}\right)^2 
+ 0.1320  \left(\kappa_1^{WW}\right)^2 
+ 0.1944  \left(g_2^{WW}\right)^2 
+ 0.08075  \left(g_4^{WW}\right)^2  
\\	
 && +\, 0.7204 \left(\frac{g_1^{WW}}{2}\right)  \kappa_1^{WW}
+ 0.7437 \left(\frac{g_1^{WW}}{2}\right)  g_2^{WW}
+ 0.2774\, \kappa_1^{WW} g_2^{WW} 
\,.
\nonumber 
\end{eqnarray}
%\end{equation}
%%%%%%%%%%%%%%%%%%%%%

For the $H\to ZZ/Z\gamma^*/\gamma^*\gamma^*\rightarrow$\,four-fermion final state, 
we set $\Lambda_1^{Z\gamma}=\Lambda_1^{ZZ}=100$\,GeV  in Eq.~(\ref{eq:HVV}) and rely on the $\kappa_2^{Z\gamma}$ and 
$\kappa_1^{ZZ}=\kappa_2^{ZZ}$ parameters to express 
%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
\label{eq:ratio-3}
R_{ZZ/Z\gamma^*/\gamma^*\gamma^*} =
&& \left(\frac{g_1^{ZZ}}{2}\right)^2 
+ 0.1695  \left(\kappa_1^{ZZ}\right)^2 
+ 0.09076  \left(g_2^{ZZ}\right)^2 
+ 0.03809  \left(g_4^{ZZ}\right)^2  
	  \\
&& +\, 0.8095 \left(\frac{g_1^{ZZ}}{2}\right)  \kappa_1^{ZZ}
+ 0.5046  \left(\frac{g_1^{ZZ}}{2}\right)  g_2^{ZZ}
+ 0.2092\,  \kappa_1^{ZZ} g_2^{ZZ} 
	\nonumber  \\
&& +\, 0.1023  \left(\kappa_2^{Z\gamma}\right)^2 
+ 0.1901 \left(\frac{g_1^{ZZ}}{2}\right)  \kappa_2^{Z\gamma}
+ 0.07429\,  \kappa_1^{ZZ} \kappa_2^{Z\gamma}
+ 0.04710\,  g_2^{ZZ} \kappa_2^{Z\gamma}
%	\nonumber  \\
%+ ..... \left(g_2^{Z\gamma}\right)^2  + ..... \left(g_4^{Z\gamma}\right)^2  + ..... \left(g_2^{\gamma\gamma}\right)^2 + ..... \left(g_4^{\gamma\gamma}\right)^2 
%	\nonumber  \\
%+ .....  \frac{g_1^{ZZ}}{2} g_2^{Z\gamma} + ..... \kappa_1^{ZZ}  g_2^{Z\gamma} + .....  g_2^{ZZ} g_2^{Z\gamma} + .....   \kappa_2^{Z\gamma} g_2^{Z\gamma}
%	\nonumber  \\
%+ .....  \frac{g_1^{ZZ}}{2} g_2^{\gamma\gamma} + ..... \kappa_1^{ZZ}  g_2^{\gamma\gamma} + .....  g_2^{ZZ} g_2^{\gamma\gamma} + .....  \kappa_2^{Z\gamma} g_2^{\gamma\gamma} + .....  g_2^{Z\gamma} g_2^{\gamma\gamma}
%	\nonumber  \\
%+ ..... g_4^{ZZ} g_4^{Z\gamma} + ..... g_4^{ZZ} g_4^{\gamma\gamma} + ..... g_4^{Z\gamma} g_4^{\gamma\gamma}
\,.
	\nonumber
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%%
We set $g_2^{Z\gamma}=g_4^{Z\gamma}=g_2^{\gamma\gamma}=g_4^{\gamma\gamma}=0$ in Eq.~(\ref{eq:ratio-3}).
These four couplings require a coherent treatment of the $q^2$ cutoff for the virtual photon and are left for
a dedicated analysis. 

For the $H\to\gamma\gamma$ and $\Z\gamma$ final states, 
we include the $W$ boson and the top and bottom quarks in the loops and obtain
%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
\label{eq:ratio-5}
R_{\gamma\gamma} = && 
1.60578\left(\frac{g_1^{WW}}{2}\right)^2 + 0.07313\,  \kappa_t^2 -0.68556\left(\frac{g_1^{WW}}{2}\right)\kappa_t
	+ 0.00002\,  \kappa_b^2 - 0.00183\, \kappa_t\kappa_b
	 \\
&&	+\, 0.00846\left(\frac{g_1^{WW}}{2}\right)\kappa_b 
	+ 0.16993\,  \tilde\kappa_t^2 + 0.00002\,  \tilde\kappa_b^2 - 0.00315\, \tilde\kappa_t\tilde\kappa_b  
	\nonumber 
	\,.
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
\label{eq:ratio-6}
R_{Z\gamma} = && 
1.118600 \left(\frac{g_1^{WW}}{2}\right)^2 + 0.003489\, \kappa_t^2 - 0.125010  \left(\frac{g_1^{WW}}{2}\right)\kappa_t
	+ 0.000003\, \kappa_b^2 - 0.000183 \kappa_t\kappa_b 
	\\
&&	+\,  0.003100 \left(\frac{g_1^{WW}}{2}\right)\kappa_b 
	+ 0.012625\, \tilde\kappa_t^2 + 0.000005\, \tilde\kappa_b^2 - 0.000467\, \tilde\kappa_t\tilde\kappa_b  
	\nonumber 
	\,.
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%%
The point-like interactions $g_2^{\gamma\gamma}$ and $g_4^{\gamma\gamma}$ or  $g_2^{Z\gamma}$ and $g_4^{Z\gamma}$ 
could be considered in Eqs.~(\ref{eq:ratio-5}) and~(\ref{eq:ratio-6}). 
However, following the approach in Eq.~(\ref{eq:ratio-3}), these are left to a dedicated analysis. 
We do not consider higher-order corrections,
such as terms involving $\kappa_{1,2}^{WW}$, $g_2^{WW}$, or $g_4^{WW}$, in Eqs.~(\ref{eq:ratio-5}) and~(\ref{eq:ratio-6}).
We also neglect the $H\to\gamma^*\gamma$ contribution. 

To conclude the discussion of the cross sections,
we note that the relative contribution of an individual coupling $a_n$, either to production 
$\left(\sum  \alpha_{jk}^{(i)}a_ja_k\right)$ or to decay $\left(\sum  \alpha_{lm}^{(f)}a_la_m\right)$,
can be parameterized as an effective cross-section fraction 
%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
 f_{an}^{(i,f)} = \frac{\alpha_{nn}^{(i,f)}a_n^2}{\sum_{m}\alpha_{mm}^{(i,f)}a_m^2}\times{\rm sign}\left(\frac{a_n}{a_1}\right)
 \,,
\label{eq:fgn}
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%%
where the sign of the $a_n$ coupling relative to the dominant SM contribution $a_1$ is incorporated into the $f_{an}$ definition. 
In the denominator of Eq.~(\ref{eq:fgn}), the sum runs over all couplings contributing to the $i\to H$ or $H\to f$ process.
By convention, the interference contributions are not included for ease of interpretation.

We adopt the definition of $f_{an}$ used by the LHC experiments~\cite{CMS-HIG-12-041,Khachatryan:2014kca,Aad:2015mxa}
for $HWW$, $HZZ$, $HZ\gamma$,  and $H\gamma\gamma$ anomalous couplings 
in the $H\to ZZ/Z\gamma^*/\gamma^*\gamma^*\to 2e2\mu$ process, with the $HWW$ couplings related 
through Eqs.~(\ref{eq:deltaMW})--(\ref{eq:kappa1WW}); 
$f_{\rm CP}^{\rm gg}$ in the ggH process for the effective $Hgg$ couplings~\cite{Anderson:2013afp}; 
%and $f_{\rm CP}^{qq}$ for processes involving $H q\bar{q}$ fermion couplings, such as $H\to q\bar{q}$ or $\ttH$,
%where by convention we use $\alpha_{\kappa\kappa}=\alpha_{\tilde{\kappa}\tilde{\kappa}}$ in Eq.~(\ref{eq:fgn}) despite the fact that
%Eq.(\ref{eq:ratio-1}) is not valid for the heavy top quark~\cite{Gritsan:2016hjl}.
and $f_{\rm CP}^{qq}$ for processes involving $H q\bar{q}$ fermion couplings, 
such as $H\to q\bar{q}$, with $\alpha_{mm}=1$ in Eq.~(\ref{eq:fgn}). 
The latter convention for $f_{\rm CP}^{tt}$ is extended to the $H t\bar{t}$ couplings as well, 
despite the fact that Eq.(\ref{eq:ratio-1}) is not valid for the heavy top quark~\cite{Gritsan:2016hjl}.
It is also easy to invert Eq.~(\ref{eq:fgn}) to relate the cross section fractions to coupling ratios via
%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
%\frac{a_n}{a_m}=\sqrt{\frac{\left|f_{an}^{(f)}\right| \alpha_{mm}^{(f)}}{\left|f_{am}^{(f)}\right| \alpha_{nn}^{(f)}  }}
%\times{\rm sign}\left(f_{an}^{(f)}f_{am}^{(f)}\right)
\frac{a_n}{a_m}=\sqrt{\frac{\left|f_{an}\right| \alpha_{mm}}{\left|f_{am}\right| \alpha_{nn}  }}
\times{\rm sign}\left(f_{an}f_{am}\right)
 \,,
\label{eq:an}
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%%
where we omit the process index for either $i\to H$ or $H\to f$.
Because $\sum_n \left|f_{an}\right|=1$, only all but one of the parameters are independent.  
We choose to use the $f_{an}$ corresponding to anomalous couplings as our independent set of parameters, 
leaving for example $f_{a1}=\left(1-\sum_{n\ne1}|f_{an}|\right)$ as a dependent one. 

There are several advantages in using the $f_{an}$ parameters in Eq.~(\ref{eq:fgn}) in analyzing a given process on the LHC.
First of all, the $f_{an}$ and signal strength $\mu^{i\to f}=\sigma^{i\to f}/\sigma^{i\to f}_{\rm SM}$ form a complete 
and minimal set of measurable parameters describing the process $i\to H\to f$. 
%There are many more couplings and their measurement is degenerate in a single process,
Measuring directly in terms of couplings introduces degeneracy in Eq.~(\ref{eq:diff-cross-section2}),
because, for example, the production couplings can be scaled up and the decay couplings down without
changing the result. A similar interplay occurs between the couplings appearing in the numerator
and the denominator of Eq.~(\ref{eq:diff-cross-section2}).
Second, the $f_{an}$ parameters are independent of ${\Gamma_{\rm tot}}$, which is absorbed 
into $\mu^{i\to f}$. In contrast, the direct coupling measurement $a_n$ depends on the assumptions 
in Eq.~(\ref{eq:width_other}), including $\Gamma_{\rm other}$. 
Third, $f_{an}$ has the same meaning in all production and all decay channels of the \Hboson.
For example, the $f_{an}$ measurement in VBF production is invariant with respect to the $H\to f$ 
decay channel used. This can be seen from Eq.~(\ref{eq:diff-cross-section2}), where 
$\left(\sum  \alpha_{lm}^{(f)}a_la_m\right)/{\Gamma_{\rm tot}}$ can be absorbed into the $\mu^{i\to f}$ 
% footnote -> 
parameter.\footnote{The situation when production and decay cannot be decoupled in analysis of the data 
due to the same couplings appearing in both processes, such as in $i\to VV\to H\to VV\to f$, is discussed 
in detail in Section ~\ref{sect:exp_onshell}.}
% 
Fourth, $f_{an}$ is a ratio of observable cross sections, and therefore it is invariant with respect to
the $a_n$ coupling scale convention. For example, the $f_{an}$ value is identical for either the $c_n$ or $g_n$ 
couplings related in Eq.~(\ref{eq:EFT_ci}). 
Fifth, in the experimental measurements of $f_{an}$ most systematic uncertainties cancel in the ratios, 
making it a clean measurement to report. 
Sixth, the $f_{an}$ are convenient parameters for presenting results as their full range is bounded 
between $-1$ and $+1$, while the couplings and their ratios are not bounded. 
Finally, the $f_{an}$ have an intuitive interpretation, as their values indicate the fractional contribution 
to the measurable cross section, while there is no convention-invariant interpretation of the coupling 
measurements. 
In the end, the measurements in individual processes can be combined, and at that point 
their interpretation in terms of couplings becomes natural. However, this becomes feasible 
only when the number of measurements is at least equal to, or preferably exceeds, the number of couplings. 

