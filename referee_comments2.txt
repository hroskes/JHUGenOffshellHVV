

Dear Dr. Heller,

Please find below our detailed answers to each point raised by the referee:

> The authors addressed most of the points raised in my report. I have
> the following suggestions:
>
> Regarding the manual: I would more prominently refer to the website
> and manual of the code already in the introduction.

We now add the following sentence at the very end of the Introduction:

"The technical details of the framework are described in the manual, which
can be downloaded at [87], together with the source code."

We have also revised the actual manual on the web page https://spin.pha.jhu.edu
referenced in [87], where some of the points raised in this review were reflected 
(such as the translation between different EFT bases). 


> Related to some comments in the report (numbers are the same as in the
> report).
>
> 2) I suggest that the authors add a footnote for clarity.

We believe that the fact that the kappa framework is a special case of our more general 
approach when all anomalous tensor structures are set to zero should be transparent from 
the notation used, so we made no modification to our text. Since the kappa framework was 
introduced in analysis of LHC data, we published Refs.[2-4], and in none of those cases 
we had to explain this special case. Therefore, it would even look strange that we suddenly 
make this comment (e.g. a footnote) after almost of a decade of discussing this framework. 
We already explain it in the paper that g_1=2 and kappa_f=1 in the SM, so it becomes trivial 
to relate these to the kappa values. 


> 5) I understand the (43) is an inversion of (42). That does not change
> the fact that both of those equations ignore interference
> contributions. If one wants to call f a cross-section fraction, that
> is true only if there is no interference. If f is by definition what
> eq. 42 says, then it's not a cross-section fraction in general. It
> works fine for some of the experimental analyses the authors cite
> exactly because they consider couplings for which there is no
> interference, such as the cp-odd and cp-even contributions...
> 
> If the authors are convinced that this parameterisation works
> independently of whether there is interference then they should state
> it by modifying: "By convention, the interference contributions are
> not included for ease of interpretation."

First of all, let us stress that we always called f_ai to be "an effective cross-section fraction,"
where the word "effective" means that it is not literally a cross-section fraction in the process, 
which cannot be defined precisely in the presence of interference. What is important in this 
definition is that the measured results can be precisely interpreted and their meaning is well-defined. 

However, we follow the suggestion from the referee and modify the quoted sentence as follows:

"By convention, the interference contributions are not included in the effective fraction 
definition in Eq.(42) so that this parameter can be more easily interpreted."


> 6) For clarity I would suggest adding a sentence in the text.

We now add this sentence just below Eq.(39):

"We note that some final states in the $H\to WW$ and $ZZ/Z\gamma^*/\gamma^*\gamma^*\rightarrow$\,four-fermion 
decay may interfere, but their fraction and phase-space overlap are very small. We therefore neglect this effect." 


In addition, we have noticed the x-axis labels in Fig. 6 (c) and (f) have been missing
the units [GeV]. Therefore, we attached a revised version of these two plots, where the
only change is addition of units. 





