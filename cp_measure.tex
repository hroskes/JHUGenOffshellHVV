
Here we present a detailed example of the $C\!P$-sensitive measurement of the $f_{a3}$ parameter,
defined in Eq.~(\ref{eq:fa3}), in the $HVV$ coupling of the Higgs boson to two weak vector bosons 
using the $H\to ZZ\to 4\ell$ decay,  with vector boson fusion, associate production with the vector bosons 
$W$ and $Z$, or inclusive production, and using both onshell and offshell production. 
The reason for picking the $H\to ZZ\to 4\ell$ decay for illustration of analysis is the rich spectrum
of production and decay kinematics to be explored. For example, only the $H\to ZZ$ and $WW$ decays
allow the measurements offshell. At the same time, only the $H\to ZZ$ decay allows full measurement 
of spin correlations in the four-lepton final state. Nonetheless, measurements can be expanded 
to other final states, for example study of 
VBF in $H\to\gamma\gamma$, $VH$ in $H\to b\bar{b}$, and offshell in $H\to 2\ell2\nu$. 

For any given set of coupling values, the $f_{a3}$ and $\phi_{a3}$ parameters~\cite{Khachatryan:2014kca} 
are defined as
%%%%%%%%%%%%%%%%%%%%%%%
\begin{equation}
f_{a3} = \frac{|{a_3}|^2 \sigma_{3}}{|{a_1}|^2 \sigma_{1} + |{a_2}|^2 \sigma_{2} + |{a_3}|^2 \sigma_{3} +\ldots}
\,,
~~~\phi_{a3} = {\rm arg}(a_3/a_1)
\,,
\label{eq:fa3}
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%
where $\sigma_{i}$ is the cross section of the $H\to ZZ\to2e2\mu$ process corresponding 
to $a_{i} = 1$, $a_{j \neq i} = 0$ in decay without modification of production,
and the $2e2\mu$ final state is not affected by interference 
between same-flavor leptons in the final state. The $f_{a3}$ parameter is independent of the collider 
energy when it is defined for decay, it is conveniently bounded between 0 and 1, uniquely defined, 
and it has the meaning of the cross section fraction corresponding to the pseudoscalar coupling in decay. 
The symmetry $a_1=a_1^{WW}$ and $a_3=a_3^{WW}$ is assumed throughout this Section,
and dependence on the cut-off scale $\Lambda_{V}$ is tested, as introduced in Eq.~(\ref{eq:formfact-spin0}) 
and with the assumption $\Lambda_{V}=\Lambda_{V1}=\Lambda_{V2}=\Lambda_{H}$. 
Both assumptions test reasonable relationship of potential new physics contributions, 
but the analysis can be easily extended for any relationship of the couplings or cut-off scales. 
Since the cut-off scale of interest $\Lambda_{V}$ is larger than the Higgs boson mass, 
the cross section ratios in Eq.~(\ref{eq:fa3}) are not affected. 
While the phase of the $a_3/a_1$ ratio can be generally complex and $q^2$-dependent, 
we will consider this ratio to be real and constant below the cutoff scale of $\Lambda_{V}$, 
that is $\phi_{a3} = 0$ or~$\pi$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}[t]
\begin{center}
\caption{
Summary of three production categories in analysis of the $H(125)\to 4\ell$ events.
The selection requirements targeting VBF or $VH$, onshell or offshell production are listed.  
The number of expected reconstructed onshell events of each type is shown for 3\,000\,fb$^{-1}$ of integrated luminosity 
at 13\,TeV LHC energy, and the typical selection efficiencies in the detector of the events produced onshell are given for each 
production mechanism in parentheses. 
The discriminants $\mathcal{D}$ based on the matrix element likelihood calculations 
are defined for each category of events as discussed in text. 
 }
\begin{tabular}{|l|c|c|c|}
\hline
%   ~~category              & VBF-jets& $VH$-jets   & Inclusive \\
%\hline\hline
 \vspace{-0.2cm}                  &                &                 &  \\
  ~~target process         &    ~~$q{q}^\prime VV\to q{q}^\prime H\to (jj)(4\ell)$~~            &      ~~$q\bar{q}\to VH\to (jj)(4\ell)$~~     &  ~~$H\to 4\ell$~~   \\
 \vspace{-0.2cm}                 &                &                 &  \\
\hline\hline
%
 \vspace{-0.2cm} & & & \\
%
  ~~dijet requirements         
              &     ~~$ \mathcal{D}_{\rm bkg}^{\rm VBF} >0.5$  or $ \mathcal{D}_{\rm bkg}^{\rm VBF, 0-} >0.5$~~ 
             &     ~~$ \mathcal{D}_{\rm bkg}^{V\!H} >0.5$  or $ \mathcal{D}_{\rm bkg}^{V\!H, 0-} >0.5$~~
              &  ~~not VBF-jets  or $VH$-jets~~ \\
%
 \vspace{-0.2cm} & & & \\
%
  ~~offshell requirements~~  & $220<m_{4\ell}<2200$\,GeV & $220<m_{4\ell}<2200$\,GeV & $220<m_{4\ell}<2200$\,GeV \\
%
 ~~observables  offshell~~
             &  $m_{4\ell}$, $ \mathcal{D}_{\rm bkg}^{\rm VBF+dec}$, $\mathcal{D}_{0-}^{\rm VBF+{\rm dec}}$
             &  $m_{4\ell}$, $ \mathcal{D}_{\rm bkg}^{V\!H+{\rm dec}}$, $\mathcal{D}_{0-}^{V\!H+{\rm dec}}$
             &  $m_{4\ell}$, $ \mathcal{D}_{\rm bkg}^{\rm dec}$, $\mathcal{D}_{0-}^{\rm dec}$ \\
%
 \vspace{-0.2cm} & & & \\
%
 ~~onshell requirements~~  & $110<m_{4\ell}<140$\,GeV & $110<m_{4\ell}<140$\,GeV & $110<m_{4\ell}<140$\,GeV \\
 %
  ~~observables  onshell
             &  $\mathcal{D}_{\rm bkg}^{\rm VBF+4\ell}$, $\mathcal{D}_{0-}^{\rm VBF+{\rm dec}}$, $\mathcal{D}_{C\!P}^{\rm VBF}$
             &  $\mathcal{D}_{\rm bkg}^{V\!H+4\ell}$, $\mathcal{D}_{0-}^{V\!H+{\rm dec}}$, $\mathcal{D}_{C\!P}^{V\!H}$
             &  $\mathcal{D}_{\rm bkg}^{4\ell}$, $\mathcal{D}_{0-}^{\rm dec}$, $\mathcal{D}_{C\!P}^{\rm dec}$  \\
 %
 \vspace{-0.2cm} & & & \\
% 
 ~~$ggH$  selection     &      Y (X\%)          &       Y (X\%)            &   Y (X\%)  \\
 ~~VBF   selection    &      Y (X\%)          &       Y (X\%)            &   Y (X\%)  \\
 ~~$WH$   selection      &      Y (X\%)          &       Y (X\%)            &   Y (X\%)  \\
 ~~$ZH$  selection     &      Y (X\%)          &       Y (X\%)            &   Y (X\%)  \\
 ~~$t\bar{t}H$   selection     &      Y (X\%)          &       Y (X\%)            &   Y (X\%)  \\
 ~~$b\bar{b}H$  selection   &      Y (X\%)          &       Y (X\%)            &   Y (X\%)  \\
 ~~$tH\bar{q}$   selection   &      Y (X\%)          &       Y (X\%)            &   Y (X\%)  \\      
                  &                &                 &  \\
\hline
%
\end{tabular}
\label{table:categories}
\end{center}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The measurement of $f_{a3}$ can be obtained from either correlations of jets in associate production, 
from kinematics in the four-lepton decay, decay rates in offshell production, or from any combination 
of the above, and therefore the full analysis requires a large number of observables. 
However, an efficient analysis 
can be performed by separating events into categories, each enhanced with a particular type of production 
process, such as for example onshell VBF production, and defining the optimal observables in each
category. Such six categories are defined in Table~\ref{table:categories}, targeting VBF, $VH$, and
inclusive production, either onshell or offshell. For illustration purpose, only hadronic decay of an associated 
vector boson is studied, while leptonic decays could be considered in the same way. Other production modes,
such as $t\bar{t}H$, $b\bar{b}H$, $tH\bar{q}$, or gluons fusion with associated jets, involve fermion couplings
and are considered elsewhere~\cite{Anderson:2013afp, Schulze:2015tth}. The four-lepton 
final state can be further split into the $4\mu$, $2e2\mu$, and $4e$ final states, leading to the total of
18 categories. The latter separation is needed for proper treatment of lepton interference with identical
leptons in the final state, though this is a small effect and the primary reason is due to differences
in detector reconstruction. 

The choice of discriminants listed in Table~\ref{table:categories} is motivated in Section~\ref{sect:cp_mela}
and by the requirement to limit the number of observables to no more than three. As an example, the
$\mathcal{D}_{\rm bkg}^{\rm VBF+4\ell}$ discriminant is designed to separate VBF onshell signal from 
background and includes kinematics in both production with two associated jets and decay to four leptons. 
It is defined as
%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
%
\label{eq:kd-ggmela}
\mathcal{D}_{\rm bkg}^{\rm VBF+4\ell} = \left[1+
\frac{
\mathcal{P}^{\rm decay}_{\rm bkg} ({\bf\Omega}^{\rm decay},{\bf\Omega}^{\rm prod})  
\cdot \mathcal{P}^{\rm VBF}_{\rm bkg} ({\bf\Omega}^{\rm VBF})  
\cdot \mathcal{P}^{\rm mass}_{\rm bkg} (m_{4\ell})  
}
{
\mathcal{P}^{\rm decay}_{\rm sig} ({\bf\Omega}^{\rm decay})  
\cdot \mathcal{P}^{\rm VBF}_{\rm sig} ({\bf\Omega}^{\rm VBF})  
\cdot \mathcal{P}^{\rm mass}_{\rm sig} (m_{4\ell})  
}
%
\right]^{-1}\,,
%
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%%
where $\mathcal{P}^{\rm decay}_{\rm bkg}$ and $\mathcal{P}^{\rm VBF}_{\rm bkg}$ represent
the dominant background processes, approximated by angular distributions in decay of
$\qqbar\to ZZ\to 4\ell$ and $gg\to H+2$\,jets, respectively. It was found that correlations of jets
in either gluon fusion Higgs boson production or in nonresonant background production is 
relatively similar and differs significantly from the signal VBF production. 
This discriminant, along with $\mathcal{D}_{\rm bkg}^{V\!H+4\ell}$ and $\mathcal{D}_{\rm bkg}^{4\ell}$,
follow Eq.~(\ref{eq:melaBkg}) and include the $4\ell$ invariant mass with the detector resolution limiting 
the separation. The analogous discriminants with the decay label instead of ${4\ell}$ do not include
$m_{4\ell}$ directly, as this observable is used separately offshell. 
The two other types of discriminants listed in Table~\ref{table:categories}, $\mathcal{D}_{0-}$ and $\mathcal{D}_{C\!P}$, 
follow Eqs.~(\ref{eq:melaAlt}) and~(\ref{eq:melaInt}) defined for the pseudoscalar alternative hypothesis.

The ultimate goal of the measurement of anomalous couplings $\vec{\zeta}=\{f_{a3} , \phi_{a3},...\}$ 
is accomplished by performing a multi-dimensional fit to match observed kinematic distributions in the above
processes to the predictions. The fit follows Ref.~\cite{Gao:2010qx} with the likelihood function ${{\cal L}_k}$ 
for $N_k$ candidate events in each category of events $k$
%%%%%%%%%%%%%%
%
\begin{equation}
{{\cal L}_k} =  \exp\left( - \sum_j n^k_{j}-n^k_{\rm bkg}  \right) 
\prod_i^{N_k} \left(  \sum_j n^k_{j} \times{\cal P}^k_{j}(\vec{x}_{i};~\vec{\zeta})  
+n^k_{\rm bkg} \times{\cal P}^k_{\rm bkg}(\vec{x}_{i})  
\right)\,,
\label{eq:likelihood}
\end{equation}
%%
%%%%%%%%%%%%%%
where $n^k_{j}$ is the number of signal events of type $j$  in category $k$,
$n^k_{\rm bkg}$ is the number of background events  in category $k$, and 
${\cal P}(\vec{x}_{i};\vec{\zeta})$ is the probability density function for signal or background
of a given type. Each candidate event  $i$ is characterized by a set of three observables
$\vec{x}_{i}=\{\mathcal{D}_1,\mathcal{D}_2,\mathcal{D}_3\}$ 
as defined in Table~\ref{table:categories}.
The signal type $j$ is one of the seven production mechanisms listed in 
Tables~\ref{table:mc_production} and~\ref{table:categories}.

Three event categories $k$ are defined in Table~\ref{table:categories},
for the VBF selection, $VH$ section, and the remaining events, which are further
split into three decay modes $H\to ZZ\to 4\mu, 2e2\mu,$ and $4\mu$, 
and into two onshell and offshell categories, giving a total of eighteen categories.
%
The total likelihood ${{\cal L}}$ to be maximized is the product of all likelihood functions ${{\cal L}_k}$,
which allows simultaneous fit of the signal properties $\vec{\zeta}$ in all event categories. 
It is important to note that the relative yield of events $n^k_{j}$ of different type and in different
categories is known for a given set of parameters $\vec{\zeta}$ from simulation. We only split
the event types by either the fermion coupling in production ($ggH$, $t\bar{t}H$, $b\bar{b}H$, $tH\bar{q}$)
or vector boson coupling in production (VBF, $WH$, $ZH$), allowing arbitrary relative strength of the
fermion and vector boson couplings. Therefore, all signal yields are described
by only two free parameters, $\mu_f$ and $\mu_V$, which are the ratios of the observed and SM expected
yields for each $n^k_{j}$, apart from the effect of the $H$ boson width in the offshell categories which 
are parameterized separately as discussed below. 
The known yield dependence on the signal parameters $\vec{\zeta}=(f_{a3}, \phi_{a3},..)$ 
is incorporated in the parameterization.
 
 Probability parameterization offshell requires special treatment. There is a sizable interference between
 the resonant signal $H\to ZZ$ and nonresonant diboson $ZZ$ production in either gluon fusion or
 vector boson initiated processes, including VBF and $VH$ production for signal. Moreover, the 
ratio of the $H$ boson yield in the offshell and onshell regions scales linearly with the width $\Gamma_H$,
which we parameterize as a ratio of $\Gamma_H/\Gamma_0$ with respect to a reference value $\Gamma_0$
used in simulation. This effect is introduced with the factor $\sqrt{\Gamma_H/\Gamma_0}$ for interference. 
The total parameterization cannot separate the signal and background parts and therefore the sum 
of all probabilities in Eq.~(\ref{eq:likelihood}) with the number of events absorbed in normalization 
is written as, following Ref.~\cite{Khachatryan:2015mma}, 
%%%%%%%%%%%%%%
%
\begin{eqnarray}
{\cal P}_{\rm tot}^{\rm offshell}(\vec{x}; \GH, \vec{\zeta})  = & 
\Biggl[
\mu_{f}\,\frac{\GH}{\Gamma_0} \, {\cal P}^{k}_{gg,\rm sig}(\vec{x}; \vec{\zeta})
+  \sqrt{\mu_{f}\,\frac{\GH}{\Gamma_0} } \, {\cal P}^{gg}_{\rm int}(\vec{x}; \vec{\zeta})
+  {\cal P}^{gg}_{\rm bkg}(\vec{x})
\Biggr] ~~~~~~~~~~~~
\nonumber \\
&  +
\Biggl[
\mu_{V}\,\frac{\GH}{\Gamma_0} \, {\cal P}^{\V\V}_{\rm sig}(\vec{x}; \vec{\zeta})
+  \sqrt{\mu_{V} \,\frac{\GH}{\Gamma_0} } \, {\cal P}^{\V\V}_{\rm int}(\vec{x}; \vec{\zeta})
+ {\cal P}^{\V\V}_{\rm bkg}(\vec{x})
\Biggr]
 + {\cal P}^{\Pq\Paq}_{\rm bkg}(\vec{x}) \,.
\label{eq:likelihood-offshell}
\end{eqnarray}
%
%%%%%%%%%%%%%%

 
%The number of observables and free parameters can be extended or reduced, depending on the desired fit. 
\vspace{2cm}

...... need to discuss effect of anomalous couplings on ${\cal P}^k_{j}$ .......





