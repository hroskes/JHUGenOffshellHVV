\subsection{Kinematics in decay of a new resonance}

While the relative strength of the di-boson final state is
a priori unknown, lets us first discuss kinematics in the decay illustrated with the example
of a hypothetical $X(750)$ state in Fig.~\ref{fig:kinematics_x750}. 
In the following, we consider the decays $X(750)\to ZZ\to 4f$, 
$X(750)\to W^+W^- \to 4f$, $X(750)\to \gamma\gamma$, and $X(750)\to Z\gamma\to ff\gamma$.
As has already been pointed out earlier, both the $Z\gamma$ and $\gamma\gamma$ final states
do not allow one to differentiate between different hypotheses of spin-zero. The production 
distributions $\cos\theta^*$ and $\Phi_1$ are trivially flat due to lack of spin correlations, 
and even the $\cos\theta_{1}$ distribution in the $X\to Z\gamma\to ff\gamma$ decay follows 
$(1+\cos\theta_{1})^2$ in any scenario, unless there is a complex phase in the mixture of 
$C\!P$-even and $C\!P$-odd contributions, see Fig.~15 in Ref.~\cite{Anderson:2013afp}.

On the other hand, production and decay of a spin-two particle $X$ leads to non-trivial 
distribution of all angular observables as shown in Fig.~\ref{fig:kinematics_x750}. 
In particular, $gg\to X$ or $q\bar{q}\to X$ production mechanisms lead to either $\pm 2$ or $\pm 1$ 
initial state polarization in the case of minimal gravity-like couplings, corresponding to the coupling
$g_1^{(2)gg}=1$ in production following notation of Ref.~\cite{Gao:2010qx}.
For a full list of spin-two models see Ref.~\cite{Gao:2010qx}. Here we consider the so-called
minimal coupling ($2_m^+$, $g_1^{(2)VV}=g_5^{(2)VV}=1$ in decay) and 
gravity propagating in the bulk ($2_b^+$, $g_5^{(2)VV}=1$ in decay) models. 
Distributions for both models are shown in Fig.~\ref{fig:kinematics_x750}.
%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
\centerline{
\epsfig{figure=plots/DecZZ_costhetastar.pdf,width=0.31\linewidth}
\epsfig{figure=plots/DecWW_costhetastar.pdf,width=0.31\linewidth}
\epsfig{figure=plots/DecZgamma_costhetastar.pdf,width=0.31\linewidth}
}
\centerline{
\epsfig{figure=plots/DecZZ_helcosthetaZ1.pdf,width=0.31\linewidth}
\epsfig{figure=plots/DecWW_helcosthetaZ1.pdf,width=0.31\linewidth}
\epsfig{figure=plots/DecZgamma_helcosthetaZ1.pdf,width=0.31\linewidth}
}
\centerline{
\epsfig{figure=plots/DecZZ_phistarZ1.pdf,width=0.31\linewidth}
\epsfig{figure=plots/DecWW_phistarZ1.pdf,width=0.31\linewidth}
\epsfig{figure=plots/DecZgamma_phistarZ1.pdf,width=0.31\linewidth}
}
\centerline{
\epsfig{figure=plots/DecZZ_helphi.pdf,width=0.31\linewidth}
\epsfig{figure=plots/DecWW_helphi.pdf,width=0.31\linewidth}
\epsfig{figure=plots/Decgammagamma_costhetastar.pdf,width=0.31\linewidth}
}
\caption{
Distributions of the observables  $\{\cos\theta^*, \cos\theta_{1,2}, \Phi_1,  \Phi\,\}$
in the $X(750)\to VV$ decay in four channels: $ZZ\to 4f$ (left column),  $W^+W^- \to 4f$ (middle column),
$Z\gamma\to ff\gamma$ (right column), with the exception that there is no $\Phi$ plot
for $Z\gamma$ and instead the $\cos\theta^*$ distribution for  $\gamma\gamma$  is shown at the bottom-right.
Six models are shown: SM $J^P=0^+$ (red circles), $0^-$ (blue diamonds),
$gg\to X$ $2_m^+$ (green open squares) and $2_b^+$ (magenta open triangles),
$q\bar{q}\to X$ $2_m^+$ (green solid squares) and $2_b^+$ (magenta solid triangles).
Generated events are shown as points and smooth lines are projections of analytical distributions. 
}
\label{fig:kinematics_x750}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%

While the above discussions focusses on kinematics, one can also draw conclusions from
the relative rates of different di-boson final states. For example, using relationship in 
Eqs.~(\ref{eq:relationshipJJ},\ref{eq:relationshipZJ},\ref{eq:relationshipZZ}), one can relate
the branching fractions, or equivalently production cross sections. 
Table~\ref{table:relatecsdecay} shows cross sections of various di-boson final states in the process 
$pp\to X(750)\to VV$ assuming that the two-photon correction is ${5}$\,fb at LHC $\sqrt{s}=13$\,TeV
under three hypotheses of the $a_2^{VV}$ couplings. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}[h]
\begin{center}
\caption{
Expected production cross section of various di-boson final states in the process 
$pp\to X(750)\to VV$ assuming that the two-photon cross section is ${5}$\,fb at LHC $\sqrt{s}=13$\,TeV
under three hypotheses of the $a_2^{VV}$ couplings. 
The $\gamma^*\to 2\ell$ final state assumes $m_{2\ell}>4$ GeV. 
 }
\begin{tabular}{|l|c|c|c|c|} 
\hline
\hline
 & $a_2^{BB}\ne0$ &  $a_2^{WW}\ne0$ & $a_2^{BB}=2a_2^{WW}\ne0$ \\ 
\hline
$X(750)\to{{} \gamma\gamma}$                                            & ${5}$\,fb & ${5}$\,fb            & ${5}$\,fb   \\
$X(750)\to{{}  Z\gamma}\to 2\ell\gamma$                            & ${{} 0.2}$\,fb & ${{} 2.2}$\,fb  & ${{} 0}$\,fb \\
$X(750)\to{{}  \gamma^*\gamma}\to 2\ell\gamma$             & ${{} 0.12}$\,fb & ${{} 0.12}$\,fb  & ${{} 0.12}$\,fb \\
$X(750)\to{{}  ZZ}\to4\ell$                                                         & ${{} 0.0020}$\,fb & ${{} 0.25}$\,fb& ${{} 0.023}$\,fb   \\
$X(750)\to{{}  Z\gamma^*}\to4\ell$                                        & ${{} 0.0024}$\,fb & ${{} 0.027}$\,fb& ${{} 0}$\,fb   \\
$X(750)\to{{}  \gamma^*\gamma^*}\to4\ell$                         & ${{} 0.0007}$\,fb & ${{} 0.0007}$\,fb& ${{} 0.0007}$\,fb   \\
$X(750)\to{{}  W^+W^-}\to 2\ell 2\nu$                                    & ${{} 0}$\,fb & ${{} 4.4}$\,fb & ${{} 0.24}$\,fb \\
\hline
\hline
\end{tabular}
\label{table:relatecsdecay}
\end{center}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\subsection{Kinematics in associated production}

Kinematics in associated production with two jets in shown in Fig.~\ref{fig:kinematics_wbf}. 
There are distinct features depending on the $gg$, $\gamma\gamma$, $Z\gamma$, $ZZ$, and $WW$ fusion,
which is reflected in the associated jet kinematics. 

%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
\centerline{
\epsfig{figure=plots/GenhelcosthetaV1_VBF.pdf,width=0.33\linewidth}
\epsfig{figure=plots/GenhelcosthetaV2_VBF.pdf,width=0.33\linewidth}
\epsfig{figure=plots/Genhelphi_VBF.pdf,width=0.33\linewidth}
}
\centerline{
\epsfig{figure=plots/GenQ_V1.pdf,width=0.33\linewidth}
\epsfig{figure=plots/GenQ_V2.pdf,width=0.33\linewidth}
}
\caption{
Distributions of the observables in the vector boson fusion jet associated production 
$\{\theta_1^{\rm VBF}, \theta_2^{\rm VBF}, \Phi^{\rm VBF}, \sqrt{-q_1^{2,\rm VBF}}, \sqrt{-q_2^{2,\rm VBF}} \}$ 
comparing $gg$, $\gamma\gamma$, $Z\gamma$, $ZZ$, and $WW$ fusion. 
}
\label{fig:kinematics_wbf}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%

We would like to stress that consistent treatment of all contributions with 
$\gamma\gamma$, $Z\gamma$, $ZZ$, and $WW$ intermediate states in weak boson fusion
is critical in a study of a new resonance, the same way as in decay of a new resonsance. 
While for the SM Higgs boson one could often neglect photon intermediate states when 
coupling to the $Z$ boson dominates, this is not generally the case when the tree-level
couplings are not present. This becomes clear when one compares
the relative rates of different di-boson pairs in the weak boson fusion. Using relationship in 
Eqs.~(\ref{eq:relationshipJJ},\ref{eq:relationshipZJ},\ref{eq:relationshipZZ}), one can relate
the production cross sections. 
Table~\ref{table:relatecswbf} shows cross sections of various di-boson initial states in the process 
$VV\to X(750)$ assuming that the total production cross section in weak boson fusion is 
${XXX}$\,fb at LHC $\sqrt{s}=13$\,TeV under three hypotheses of the $a_2^{VV}$ couplings. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}[h]
\begin{center}
\caption{
Expected $X(750)$ production cross section with various di-boson initial states in the process 
$VV\to X(750)$ assuming that the total production cross section in weak boson fusion is 
${XXX}$\,fb at LHC $\sqrt{s}=13$\,TeV under three hypotheses of the $a_2^{VV}$ couplings. 
({\it Need to discuss cuts on $\gamma^*$ here...})
 }
\begin{tabular}{|r|c|c|c|c|} 
\hline
\hline
 & $a_2^{BB}\ne0$ &  $a_2^{WW}\ne0$ & $a_2^{BB}=2a_2^{WW}\ne0$ \\ 
\hline
$\gamma\gamma \to X(750)$~ & $X$\,fb & $X$\,fb                   & $X$\,fb   \\
$Z\gamma\to X(750)$~             & $X$\,fb & $X$\,fb  & $X$\,fb \\
$ZZ\to X(750)$~                         & $X$\,fb & $X$\,fb& $X$\,fb   \\
$W^+W^-\to X(750)$~               & $X$\,fb & $X$\,fb & $X$\,fb \\
\hline
\hline
\end{tabular}
\label{table:relatecswbf}
\end{center}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Some older part of writeup is here, to be udpated}


Should a new boson resonance be observed on LHC, for example if the hints reported
around the mass of 750 GeV by the ATLAS~\cite{ATLAS-CONF-2015-081} and 
CMS~\cite{CMS-PAS-EXO-15-004} collaborations lead to a signal, techniques developed
for the study of the $H(125)$ boson would apply to the new state $X(m_X)$. 

First of all, one would need to determine the spin and parity quantum numbers of the new
state in all accessible final states. 
While the relative strength of the di-boson final state is
a priori unknown, lets us first discuss kinematics in the decay illustrated with the example
of a hypothetical $X(750)$ state in Fig.~\ref{fig:kinematics_x750}. 
In the following, we consider the decays $X(750)\to ZZ\to 4f$, 
$X(750)\to W^+W^- \to 4f$, $X(750)\to \gamma\gamma$, and $X(750)\to Z\gamma\to ff\gamma$.
As has already been pointed out earlier, both the $Z\gamma$ and $\gamma\gamma$ final states
do not allow one to differentiate between different hypotheses of spin-zero. The production 
distributions $\cos\theta^*$ and $\Phi_1$ are trivially flat due to lack of spin correlations, 
and even the $\cos\theta_{1}$ distribution in the $X\to Z\gamma\to ff\gamma$ decay follows 
$(1+\cos\theta_{1})^2$ in any scenario, unless there is a complex phase in the mixture of 
$C\!P$-even and $C\!P$-odd contributions, see Fig.~15 in Ref.~\cite{Anderson:2013afp}.

On the other hand, production and decay of a spin-two particle $X$ leads to non-trivial 
distribution of all angular observables as shown in Fig.~\ref{fig:kinematics_x750}. 
In particular, $gg\to X$ or $q\bar{q}\to X$ production mechanisms lead to either $\pm 2$ or $\pm 1$ 
initial state polarization in the case of minimal gravity-like couplings, corresponding to the coupling
$g_1^{(2)gg}=1$ in production following notation of Ref.~\cite{Gao:2010qx}.
For a full list of spin-two models see Ref.~\cite{Gao:2010qx}. Here we consider the so-called
minimal coupling ($2_m^+$, $g_1^{(2)VV}=g_5^{(2)VV}=1$ in decay) and 
gravity propagating in the bulk ($2_b^+$, $g_5^{(2)VV}=1$ in decay) models. 
Distributions for both models are shown in Fig.~\ref{fig:kinematics_x750}.

An important aspect in the study of a new resonance is its width. Should the width be sizable,
or should one try to set a limit on the width which is comparable to detector resolution, interference
between the signal resonance $X\to VV$ and continuum background production of $VV$ becomes
important when both original from the same production mechanism. In the following we study
this in the case of $gg\to X\to ZZ\to 4f$ resonance production, but the idea applies to any initial
and final state, as long as they are common for signal and background. Here we illustrate an
application of the framework of MCFM+JHUGen discussed in Section~\ref{sect:cp_mc}. 

An applications of off-shell $H(125)$ simulation is shown in Fig.~\ref{fig:MCFM_BSM_GenLevel}.
A hypothetical $X(m_X)$ resonance interferes with both $H(125)$ off-shell tail and 
the $gg\to4\ell$ background. The most general $HVV$ and $XVV$ couplings discussed above are possible. 
It is interesting to observe that interference of $X(m_X)$ with $H(125)$ off-shell tail and with 
$gg\to4\ell$ background have opposite sign and partially cancel each other, but the net effect
still remains and alters the distributions when ignoring the interference. 

%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
\centerline{
\epsfig{figure=plots/costheta1_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/costheta1_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/costheta1_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
}
\centerline{
\epsfig{figure=plots/costheta1_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/costheta2_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/costheta1_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
}
\centerline{
\epsfig{figure=plots/phi_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/phi_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/costheta1_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
}
\centerline{
\epsfig{figure=plots/phi_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/phi_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/phi_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
}
\caption{
Distributions of the observables  $\{\cos\theta^*, \cos\theta_{1,2}, \Phi_1,  \Phi\,\}$
in the $X(750)\to VV$ decay in four channels:
$ZZ\to 4f$ (left column), 
$W^+W^- \to 4f$ (middle column),
$\gamma\gamma$  ($\cos\theta^*$ in the top right plot),
$Z\gamma\to ff\gamma$ ($\cos\theta^*$, $\cos\theta_{1}$, $\Phi_1$ in the rest of the right column).
Six models are shown: SM $J^P=0^+$ (red circles), $0^-$ (blue diamonds),
$gg\to X$ $2_m^+$ (green open squares) and $2_b^+$ (magenta open triangles),
$q\bar{q}\to X$ $2_m^+$ (green solid squares) and $2_b^+$ (magenta solid triangles).
Generated events are shown as points and smooth lines are projections of analytical distributions. 
}
\label{fig:kinematics_x750}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
\centering
\centerline{
\includegraphics[width=0.85\textwidth]{plots/jhugen_highmass.pdf}
}
\caption{
Differential cross section of the process 
$gg \to ZZ/Z\gamma^*/\gamma^*\gamma^*\to 2\ell 2\ell^\prime$ (where $\ell$, $\ell^\prime= e$, $\mu,$ or $\tau$)
as a function of invariant mass $m_{4\ell}$ generated with the MCFM+JHUGen framework, including
the NNLO in QCD weights calculated with MCFM+HNNLO. 
The distribution is shown in the presence of a hypothetical $X(450)$ resonance with several
components either isolated or combined. In all cases interference (I) of all contributing amplitudes is included. 
}
\label{fig:MCFM_BSM_GenLevel}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%
