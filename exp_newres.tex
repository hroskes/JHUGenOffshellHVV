The techniques developed for the study of the $H(125)$ boson would apply to a search for or a study of a new 
resonance $X(m_X)$ which may arise in the extensions of the SM,
such as any Singlet model or Two Higgs Doublet model. 
For example, if any enhancement or modification of the di-boson spectrum or kinematics in the off-shell region is observed,
one would have to determine the source of this effect.
For example, it might come from a modification of the \Hboson width;
a modification of the \Hboson couplings in the off-shell region, including anomalous tensor
structures; a modification of the continuum production, possibly from anomalous self-interactions;
or yet another resonance $X$ with a larger mass. This latter scenario is necessary 
to consider in order to complete the experimental studies. 

If a new state $X$ is observed, one would need to determine its spin and parity quantum 
numbers in all accessible final states. The techniques discussed in Section~\ref{sect:exp_onshell}
would be directly relevant. If the width of the resonance is sizable, interference with background, 
as discussed in Section~\ref{sect:exp_offshell}, will become relevant. 
Moreover, interference with the off-shell $H(125)$ boson tail would become important as well. 
All these effects are included in the coherent framework of the JHU generator 
with the modified MCFM matrix element library, and are available in the MELA package 
for MC re-weighting and optimal discriminant calculations. 
They have been employed in analyses of Run-II of LHC data~\cite{Sirunyan:2018qlb,Sirunyan:2019pqw}.

Applications of off-shell $H(125)$ simulation with an additional broad $X(m_X)$ resonance
are shown in Figs.~\ref{fig:MCFM_BSM_GenLevel}, \ref{fig:MCFM_BSM_GenLevel_2}, and~\ref{fig:VBF_BSM_GenLevel}.
The cross-section of the generated resonance $X$ corresponds to the limit obtained by the recent CMS search~\cite{Sirunyan:2018qlb},
which includes all interference effects of a broad resonance. 
%Hypothetical $X(m_X)$ resonances in gluon fusion or electroweak production
%interfere with both the $H(125)$ off-shell tail and the continuum background. 
The most general $XVV$ and $Xgg$ couplings discussed in application to $HVV$ and $Hgg$ 
in Sections~\ref{sect:exp_onshell} and~\ref{sect:exp_offshell} are possible. 
It is interesting to observe that in the scalar case, the interference of $X(m_X)$ with the $H(125)$ off-shell tail and
its interference with the background have opposite signs and partially cancel each other, but the net effect
still remains and alters the distributions. In the cases of anomalous $XVV$ couplings, 
the size of the interference changes, and in some particular cases, such as the $g_4$ coupling, 
even the sign of the interference flips.  
The point-like $Xgg$ couplings are also tested and shown in Fig.~\ref{fig:MCFM_BSM_GenLevel_2},
which models the scenario when new heavy states in the gluon fusion loop are responsible for 
production of the new state $X$. 

%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
\centering
	\includegraphics[width=0.48\textwidth]{plots/h_m_a1.pdf}
\captionsetup{justification=centerlast}
\caption{
Differential cross section of the gluon fusion process 
% $gg \to ZZ/Z\gamma^*/\gamma^*\gamma^*\to 2\ell 2\ell^\prime$ (where $\ell$, $\ell^\prime= e$, $\mu,$ or $\tau$)
$gg \to ZZ/Z\gamma^*/\gamma^*\gamma^*\to 4\ell$ as a function of invariant mass $m_{4\ell}$ generated with 
JHUGen+MCFM at LO in QCD. 
The distribution is shown in the presence of a hypothetical scalar $X(450)$ resonance with SM-like couplings,
$m_X=450$\,GeV, and  $\Gamma_X=47$\,GeV.
Several components are either isolated or combined as indicated in the legend.
Interference (I) of all contributing amplitudes is included. 
%The cross-section of the resonance corresponds to the limit obtained 
% by the CMS search~\cite{Sirunyan:2018qlb}. In all cases interference (I) of all contributing amplitudes is included. 
%The upper plot shows the scalar $X$ hypothesis, and the lower plots show the $XVV$ $f_{g2}=1$ (left) and 
%$f_{g4}=1$ (right) hypotheses. In the latter two cases, distributions assuming point-like $ggX$ production are also shown. 
}
\label{fig:MCFM_BSM_GenLevel}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
\centering
	\includegraphics[width=0.48\textwidth]{plots/h_m_a2_ka.pdf}
	\includegraphics[width=0.48\textwidth]{plots/h_m_a3_kt.pdf}
\captionsetup{justification=centerlast}
\caption{
Same as Fig.~\ref{fig:MCFM_BSM_GenLevel}, but for the anomalous couplings of a new resonance $X(450)$.
A scalar resonance with $f_{g2}=1$ (left) and a pseudoscalar resonance with $f_{g4}=1$ (right) are considered. 
Both a top loop and a point-like interaction are considered in the gluon fusion production. 
}
\label{fig:MCFM_BSM_GenLevel_2}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
\centering
	\includegraphics[width=0.48\textwidth]{plots/GenHMass_a1.pdf}
	\includegraphics[width=0.48\textwidth]{plots/GenHMass_a2.pdf}
\captionsetup{justification=centerlast}
\caption{
Differential cross section of the electroweak production process 
$qq \to qq(ZZ/Z\gamma^*/\gamma^*\gamma^*\to 4\ell)$
%$VV \to ZZ/Z\gamma^*/\gamma^*\gamma^*\to 2\ell 2\ell^\prime$ (where $\ell$, $\ell^\prime= e$, $\mu,$ or $\tau$)
as a function of invariant mass $m_{4\ell}$ generated with JHUGen+MCFM. 
The distribution is shown in the presence of a hypothetical $X(1000)$ resonance with SM-like couplings (left)
and anomalous couplings ($f_{g2}=1$ and $f_{g4}=1$, right), $m_X=1000$\,GeV, and  $\Gamma_X=100$\,GeV.
Several components are either isolated or combined as indicated in the legend.
Interference (I) of all contributing amplitudes is included. 
%The cross-section of the resonance corresponds to the limit obtained by the CMS search~\cite{Sirunyan:2018qlb}.
}
\label{fig:VBF_BSM_GenLevel}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%
