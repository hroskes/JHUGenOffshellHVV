set -euxo pipefail

for pdf in fig01b fig14b; do
  gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.5 -o ${pdf}.pdf ${pdf}-pdf1.7.pdf 
done
