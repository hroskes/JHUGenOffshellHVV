
%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\centerline{
\includegraphics[width=0.55\linewidth]{plots/angles-cphvv.pdf}
}
\caption{
Illustrations of an $H$ boson production and decay in three topologies:
(1) gluon fusion $g_3g_4\to H \to V_1V_2 \to (f_{11}f_{12}) (f_{21}f_{22})$;
(2) vector boson fusion $q_{12}q_{22}\to V_1V_2 (q_{11}q_{21}) \to H(q_{11}q_{21}) \to V_3V_4(q_{11}q_{21})$;
and (3) associated production $q_{11}q_{12}\to V_1 \to V_2H\to  (f_{21}f_{22})H \to  (f_{21}f_{22}) V_3V_4$.
In the latter two cases, the decay $H \to V_3V_4$ may be followed by the same four-body decay chain as shown
in the first case. Five angles fully characterize the orientation of the production or decay chain and are defined 
in the suitable rest frames~\cite{Gao:2010qx,Anderson:2013afp}.
The only exception is the VBF production where the $\theta_i$ angles are defined
in the $H$ rest frame instead of the $V_i$ rest frames. 
}
\label{fig:kinematics}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%

Kinematic distributions of particles produced in the $H$ decay and in association with it
are sensitive to the quantum numbers and anomalous couplings of the $H$ boson. 
In the $1\to4$ process of the $H\to VV \to 4f$ decay, six observables fully
characterize kinematics of the decay products
${\bf\Omega}^{\rm decay}=\{\theta_1, \theta_2, \Phi, m_1, m_2, m_{4f} \}$,
while two other angles relate orientation of the decay frame with respect to the production axis,
${\bf\Omega}^{\rm prod}=\{\theta^*, \Phi_1 \}$, see Ref.~\cite{Gao:2010qx} and Fig.~\ref{fig:kinematics}.
Moreover, in a production process a similar set of observables could be defined, such as 
${\bf\Omega}^{\rm assoc}=\{\theta_1^{\rm VBF}, \theta_2^{\rm VBF}, \Phi^{\rm VBF}, q_1^{2,\rm VBF}, q_2^{2,\rm VBF} \}$
for the VBF process or
${\bf\Omega}^{\rm assoc}=\{\theta_1^{V\!H}, \theta_2^{V\!H}, \Phi^{V\!H}, q_1^{2,V\!H}, q_2^{2,V\!H} \}$
for the $V\!H$ process, see Fig.~\ref{fig:kinematics} and Ref.~\cite{Anderson:2013afp}. 
As a result, in the $2\to 6$ process of associated $H$ boson production (either VBF or $V\!H$) and 
its subsequent decay to a four-fermion final state, 13 kinematic observables are defined. 
Some of the on-shell $H$ boson kinematic distributions are shown in Fig.~\ref{fig:kinematics}.
There is also the overall boost of the six-body system which arises, for example, from the longitudinal
boost of the incoming partons or the transverse effects with QCD radiation, but we decouple these
hadronic effects from further consideration. 

The distributions of the two sets of observables defined either in decay, ${\bf\Omega}^{\rm decay}$, 
or in associated production, ${\bf\Omega}^{\rm assoc}$, are sensitive to the $HVV$ couplings 
in the corresponding processes shown in Fig.~\ref{fig:feynman}.
The ${\bf\Omega}^{\rm prod}$ angles are random for a production of a spin-zero particle, but provide 
non-trivial information to distinguish signal from either background or alternative spin hypotheses. 
Only a reduced set of observables is available when there are no associated particles in production, 
such as $gg\to H\to VV \to 4f$ process without associated particles or when information about
such particles is lost, or if there is no sequential decay, 
such as $VVqq^\prime\to Hqq^\prime\to\gamma\gamma qq^\prime$. 
A similar set of observables ${\bf\Omega}^{\rm assoc}$ can be defined for the strong boson fusion
with two associated partons by analogy with the VBF process, but we leave the study of this
process to our earlier work~\cite{Anderson:2013afp}. 
Note that for the production processes we define $q_i^2$ for each vector boson, where $q_i^2>0$ for $V\!H$
and $q_i^2<0$ for VBF, which also means that in VBF the $\theta_i^{\rm VBF}$ angles cannot be defined
in the rest frame of the vector bosons in their fusion and instead the $H$ frame is used. 

%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
\centerline{
\epsfig{figure=plots/costheta1_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/costheta1_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/costheta1_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
}
\centerline{
\epsfig{figure=plots/costheta2_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/costheta2_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/costheta2_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
}
\centerline{
\epsfig{figure=plots/phi_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/phi_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/phi_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
}
\centerline{
\epsfig{figure=plots/z1mass_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/z1mass_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/z1mass_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
}
\centerline{
\epsfig{figure=plots/z2mass_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/z2mass_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
\epsfig{figure=plots/z2mass_125GeV_spin0_3in1.pdf,width=0.25\linewidth}
}
\caption{
Distributions of the observables in the $H\to ZZ\to 4\ell$ analysis
of the on-shell Higgs boson with mass $m_H=125$\,GeV at the LHC with 13 TeV proton collision energy
in the decay $\{\theta_1, \theta_2, \Phi, m_1, m_2\}$ (left),
in the VBF jet associated production 
$\{\theta_1^{\rm VBF}, \theta_2^{\rm VBF}, \Phi^{\rm VBF}, \sqrt{-q_1^{2,\rm VBF}}, \sqrt{-q_2^{2,\rm VBF}} \}$ (middle),
and in the $V\!H$ associated production
$\{\theta_1^{V\!H}, \theta_2^{V\!H}, \Phi^{V\!H}, \sqrt{q_1^{2,V\!H}}, \sqrt{q_2^{2,V\!H}} \}$ (right).
The signal hypotheses are simulated with the JHUGen program
and shown for SM $0^+$ (red circles), $0^-$ (blue diamonds),
and 50\% mixture of the two contributions (green squares).
For comparison, the SM hypothesis is also shown with the POWHEG simulation. 
In the case of decay and $V\!H$ associated production observables, 
analytical parameterization is compared to numerical simulation. 
}
\label{fig:kinematics_onshell}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%

With up to 13 observables, ${\bf\Omega}$, sensitive to the Higgs boson anomalous couplings 
in Eq.~(\ref{eq:formfact-fullampl-spin0-2}),
it is a challenging task to perform an optimal analysis in a multidimensional space of observables. 
The matrix element likelihood approach (MELA) introduced 
earlier~\cite{Gao:2010qx,Bolognesi:2012mm,Anderson:2013afp} is designed to reduce the number
of observables to the minimum while retaining all essential information. Three types
of discriminants were defined for either the production or decay process, and here we generalize 
it for any sequential process of both production and decay. 

Typically there is a dominant background (bkg) which we want to separate from the SM signal (sig)
and the following discriminant is constructed as the likelihood ratio
%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
\label{eq:melaBkg}
{D_{\rm bkg}}({\bf\Omega})=
\frac{{\cal P}_{\rm sig}}{{\cal P}_{\rm sig}+{\cal P}_{\rm bkg}}
=\left[1+\frac{{\cal P}_{\rm bkg} ({\bf\Omega}^{\rm assoc},{\bf\Omega}^{\rm prod},{\bf\Omega}^{\rm decay}) }
{{\cal P}_{\rm sig} ({\bf\Omega}^{\rm assoc},{\bf\Omega}^{\rm decay}) } \right]^{-1}
%{{\cal P}_{\rm sig} ({\bf\Omega}^{\rm assoc},{\bf\Omega}^{\rm prod},{\bf\Omega}^{\rm decay}) } \right]^{-1}
\,.
%
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%% 
Such a discriminant defined for either production or decay process
facilitated the Higgs boson discovery~\cite{}
and has been used extensively in the study of its properties~\cite{}.
Here we extend it to combine both production and decay kinematics. 

Any of the anomalous couplings in Eq.~(\ref{eq:formfact-fullampl-spin0-2}) would lead to
an alternative signal hypothesis (alt) and the corresponding discriminant to isolate such
signal is defined as 
%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
\label{eq:melaAlt}
{D_{\rm alt}}({\bf\Omega})=
\frac{{\cal P}_{\rm sig}}{{\cal P}_{\rm sig}+{\cal P}_{\rm alt}}
=\left[1+\frac{{\cal P}_{\rm alt} ({\bf\Omega}^{\rm assoc},{\bf\Omega}^{\rm decay}) }
{{\cal P}_{\rm sig} ({\bf\Omega}^{\rm assoc},{\bf\Omega}^{\rm decay}) } \right]^{-1}
\,.
%
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%% 

Moreover, Eqs.~(\ref{eq:formfact-fullampl-spin0}) and~(\ref{eq:formfact-fullampl-spin0-2}) 
allow for interference of the amplitudes corresponding to the SM signal and the alternative signal. 
The following discriminant is optimal for retaining information about the interference
%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
\label{eq:melaInt}
{D_{\rm int}}({\bf\Omega})=
\frac{{\cal P}_{\rm int}({\bf\Omega}^{\rm assoc},{\bf\Omega}^{\rm decay})}
{{\cal P}_{\rm sig}({\bf\Omega}^{\rm assoc},{\bf\Omega}^{\rm decay})+{\cal P}_{\rm alt}({\bf\Omega}^{\rm assoc},{\bf\Omega}^{\rm decay})}
\,,
%
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%% 
where ${\cal P}_{\rm int}$ represents the probability distribution for a given interference term
and may in general be positive or negative. 

We would like to point out that in fact there are three interference discriminants
when anomalous couplings appear both in production and in decay.
Considering only $g_1$ and $g_4$ couplings in 
Eqs.~(\ref{eq:formfact-fullampl-spin0}) and~(\ref{eq:formfact-fullampl-spin0-2}), 
which appear once in production and once in decay sequence, the total amplitude
would have five terms proportional to $(g_4/g_1)^N$ with $N=0,1,2,3,4$. 
This allows for four independent ratios of probabilities. Equation~(\ref{eq:melaAlt})
corresponds to the ratio of the $N=4$ and $N=0$ terms. The ratios of the two odd terms 
($N=1,3$) to the zeroth term correspond to the $C\!P$-sensitive observables.
The ratio of the $N=2$ term to the $N=0$ term leads to an interference discriminant
which is similar to the one defined in Eq.~(\ref{eq:melaAlt}). In fact the four discriminants
may be re-arranged into two discriminants of the form in Eq.~(\ref{eq:melaAlt})
and two of the form in Eq.~(\ref{eq:melaInt}), in each case one observable defined 
purely for the production process and the other for the decay process. 
For example, the interference discriminants would read
%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
\label{eq:melaInt_associate}
{D^{\rm assoc}_{\rm int}}({\bf\Omega^{\rm assoc}})=
\frac{{\cal P}_{\rm int}({\bf\Omega}^{\rm assoc})}
{{\cal P}_{\rm sig}({\bf\Omega}^{\rm assoc})+{\cal P}_{\rm alt}({\bf\Omega}^{\rm assoc})}
\,, \\
\label{eq:melaInt_decay}
{D^{\rm decay}_{\rm int}}({\bf\Omega^{\rm decay}})=
\frac{{\cal P}_{\rm int}({\bf\Omega}^{\rm decay})}
{{\cal P}_{\rm sig}({\bf\Omega}^{\rm decay})+{\cal P}_{\rm alt}({\bf\Omega}^{\rm decay})}
\,.
%
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%% 

The optimal analysis of the five discriminants may not be efficient with too many correlated
observables. However, a subset of these discriminants may contain most of the information,
depending on the situation. For example, when measuring small anomalous contributions
$|g_4/g_1|\ll1$, the lowest powers $N=1,2$ may provide most of the discriminating power. 
On the other hand, when such small contributions are out of reach in statistical analysis, 
observables defined in associated production may be more powerful due to more 
pronounced effects of anomalous contributions on kinematics and cross sections. 
Such discriminants defined separately in the decay process have been used in the 
measurement of anomalous couplings of the Higgs boson on LHC~\cite{Khachatryan:2014kca},
and they still remain the optimal observables when associated particles are not reconstructed. 
However, when both associated and decay products are fully reconstructed, we find that 
the set of three discriminants ${D_{\rm bkg}}, {D_{\rm alt}}, {D^{\rm assoc}_{\rm int}}$ 
defined in Eqs.~(\ref{eq:melaBkg}),~(\ref{eq:melaAlt}), and~(\ref{eq:melaInt_associate}) 
provides most optimal information for the near-term LHC measurements. 

Most of the above discriminants are conveniently arranged to have the range from $0$ to $1$,
while the interference discriminant is bounded between $-1$ and $+1$.
%With one background process, which does not interfere with signal, and two real couplings in the 
%signal amplitude, these three discriminants retain all information needed for the measurements. 
All probabilities ${\cal P}$ are calculated using the matrix element library summarized in 
Tables~\ref{table:mc_production} and~\ref{table:mc_decay}, and the interference probability
is computed with the linear combination of the probabilities with and without the interference. 

In the case when presence of more than one anomalous
coupling is tested, when more than one background or one signal production process are present,
or when interference of signal and background become sizable, additional discriminants are easily
defined by analogy. When it becomes impractical to define larger number of
observables, it is typically possible to retain most of the relevant information with the limited
number of discriminants similar to the above. For example, multiple production processes 
can be separated into different categories of events, with each category having its own set
of discriminants. Among the multiple background processes there is typically one dominant 
process and a single discriminant would be sufficient for isolation of all backgrounds.  
%Between the two discriminants in Eqs.~(\ref{eq:melaAlt})~and~(\ref{eq:melaInt}), usually 
%one provides most of the discrimination power, depending on the size of the anomalous
%coupling tested. 

Detector resolution effects may come into play in experimental analysis and those could
be parameterized with transfer functions. However, in most practical applications there is
little effect of ignoring such resolution effects on performance when distributions of angular and 
mass observables are broad compared to resolution. This is the case for most of the observables 
shown in Fig.~\ref{fig:kinematics} for example. However, distribution of the invariant mass 
of the $H$ boson becomes significantly broader due to reconstruction effects and therefore
requires special treatment. For example in the $H\to ZZ\to 4\ell$ analysis, 
the $m_{4\ell}$ reconstructed mass could be treated as a separate observable, 
or incorporated into the ${D_{\rm bkg}}$ discriminant in Eq.~(\ref{eq:melaBkg}) where the 
${\cal P}_{\rm sig}$ and ${\cal P}_{\rm bkg}$ probabilities would also include empirical
probability for the $m_{4\ell}$ distributions. 
In Fig.~\ref{fig:kinematics-offshell} examples of such $m_{4\ell}$ distributions are
shown, including sizable off-shell $H^*$ boson production within SM and with 
anomalous contributions. 

A concrete example of the measurement of the $C\!P$ violating contribution
in the Higgs boson couplings to weak vector bosons is discussed in the next Section. 
However, the tools and approaches presented here have a wide range of applications
beyond this one measurement. 

%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
\centerline{
\epsfig{figure=plots/jhugen-offshell.pdf,width=0.5\linewidth}
\epsfig{figure=plots/jhugen-offshell.pdf,width=0.5\linewidth}
}
\caption{
Distributions of the observables in the $H\to ZZ\to 4\ell$ analysis
of the off-shell Higgs boson production in gluon fusion (left) and 
VBF+$V\!H$ produciton (right: {\it to be updated})
at the LHC with 13 TeV proton collision energy.
The signal hypotheses are simulated with the MCFM+JHUGen programs.
??? Analytical parameterization may be compared to numerical simulation (???). 
}
\label{fig:kinematics-offshell}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%



