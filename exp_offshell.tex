We continue by investigating the off-shell production and decay of the \Hboson with its coupling to either
strong or weak vector bosons. There have already been previous studies of the anomalous $HVV$ couplings 
using these tools, with the most extensive analyses from CMS~\cite{Khachatryan:2015mma,Sirunyan:2019twz}. 
Here we document and extend these studies, in particular to anomalous $Hgg$ couplings and to
anomalous couplings in background processes, which do not include the \Hboson propagator, using the EFT relationship. 
We introduced the \offshell\ effect in Section~\ref{sect:appendix_XS} and discussed simulation and analysis 
tools in Section~\ref{sect:cp_mc}. 
The special feature of \offshell\ production is the strong interference between the signal processes, which involve the \Hboson,
and background processes due to the broad invariant-mass distributions of the off-shell \Hboson. 
Analysis of the \offshell\ region is particularly important to constrain
couplings directly, without the complication of the width dependence that appears \onshell\ in Eq.~(\ref{eq:diff-cross-section2}).
Equivalently, a joint analysis of the \onshell\ and \offshell\ regions leads to constraints on
$\Gamma_{\rm other}$ in Eq.~(\ref{eq:width_other}).
Moreover, the higher $q^2$ transfer in the off-shell topology can enhance the effects of anomalous couplings.
In this Section, we set $\Gamma_{\rm tot}=4.07$\,MeV~\cite{Heinemeyer:2013tqa} and use the pole mass scheme in the gluon fusion loop 
calculations with $m_{t}=173.2$\,GeV and $m_{b}=4.75$\,GeV~\cite{Heinemeyer:2013tqa, Agashe:2014kda}.

%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[b]
\centerline{
\epsfig{figure=plots/cCompare_MCFM_Offshell_ggH_Sig_ggh_Kappa_1_Like_GenHMass__2l2l.pdf,width=0.35\linewidth}
\epsfig{figure=plots/cCompare_MCFM_Offshell_ggH_BSI_ggh_Kappa_1_Like_GenHMass__2l2l.pdf,width=0.35\linewidth}
}
\captionsetup{justification=centerlast}
\caption{
The invariant mass distribution of four-lepton events produced through gluon fusion at the LHC with a 13\,TeV proton collision energy. 
The different CP-even anomalous $Hgg$ couplings are simulated with JHUGen+MCFM at LO in QCD, and the NNLO k~factor
 is calculated with the HNNLO program, assuming signal and background k~factors to be the same as for the SM \Hboson. 
% The four-lepton invariant mass distributions in gluon fusion production and $4\ell$ decay at the LHC with a 13 TeV 
% proton collision energy. The CP-even anomalous $Hgg$ couplings are simulated with JHUGen+MCFM at LO in QCD,
% and the NNLO k factor is calculated with the HNNLO program, assuming signal and background k factors to be the same
% as for the SM \Hboson. 
Four off-shell scenarios are shown with the couplings chosen to match the SM on-shell $gg\to H\to 4\ell$ cross section:
SM (solid black), top-quark only (magenta), bottom-quark only (red), and a point-like effective interaction (blue)
shown in Eq.~(\ref{eq:g2gg}).
The left plot shows the $gg\to H\to 4\ell$ process with only signal, while the right plot includes interference with the 
SM $gg\to 4\ell$ background, which is also shown separately in the dotted histogram.
}
\label{fig:kinematics-ggH-offshell-1}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\centerline{
\epsfig{figure=plots/cCompare_MCFM_Offshell_ggH_Sig_ggh_KappaTilde_1_Like_GenHMass__2l2l.pdf,width=0.35\linewidth}
\epsfig{figure=plots/cCompare_MCFM_Offshell_ggH_BSI_ggh_KappaTilde_1_Like_GenHMass__2l2l.pdf,width=0.35\linewidth}
}
\captionsetup{justification=centerlast}
\caption{
The four-lepton invariant mass distributions in gluon fusion production as in Fig.~\ref{fig:kinematics-ggH-offshell-1}, 
but with the three CP-odd anomalous couplings instead, also chosen to match the SM on-shell $gg\to H\to 4\ell$ cross section.
% indicated in the legend shown instead.
%The four-lepton invariant mass distributions in the gluon fusion production and $4\ell$ decay as in Fig.~\ref{fig:kinematics-ggH-offshell-1},
%but with the three CP-odd anomalous couplings, as indicated in the legend, shown instead. 
}
\label{fig:kinematics-ggH-offshell-2}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%

\subsection{\Offshell\ effects due to Hgg anomalous couplings}
\label{offshell_hgg}

The \offshell\ production of the \Hboson may provide a way to disentangle contributions to the gluon fusion loop
from either SM-like couplings to the top and bottom quarks, CP-odd couplings of the \Hboson, or new heavy
particles. We illustrate this in Fig.~\ref{fig:kinematics-ggH-offshell-1} for CP-even couplings and in 
Fig.~\ref{fig:kinematics-ggH-offshell-2} for CP-odd couplings. In order to illustrate the effects, we separate 
the signal contributions from the top quark, the bottom quark, and an effective point-like interaction, with both CP-even
and CP-odd couplings to the \Hboson. 
For illustration, the SM values of the $HVV$ couplings and of the \Hboson width $\Gamma_{\rm tot}$ are assumed,
but variations of these couplings are considered in Section~\ref{offshell_hvv},
and a simultaneous measurement with $\Gamma_{\rm tot}$ can be considered. 
The tools allow the modeling of the gluon fusion loop with all possible
couplings contributing simultaneously, including interference with the background $gg\to4\ell$ process. 
The effective point-like interaction is equivalent to heavy 
$t^\prime$ and $b^\prime$ quarks in the loop, and this can also be configured in the JHU generator,
with adjustable masses of the these new particles in the loop.  

While in Section~\ref{sect:exp_onshell} it was shown how the CP-even and CP-even couplings in the gluon fusion 
loop can be separated by analyzing of \onshell\ \Hboson production in association with two jets, this approach does
not allow us to resolve different contributions to the loop. Off-shell production provides a way
to separate those contributions. As can be seen in Figs.~\ref{fig:kinematics-ggH-offshell-1} and~\ref{fig:kinematics-ggH-offshell-2},
the top quark contribution has a distinctive threshold enhancement around $2m_t$, with somewhat different 
behavior of the CP-even and CP-odd components. The light particle contribution is highly suppressed, 
and the heavy particle contribution proceeds without the $2m_t$ threshold. 
In all cases, the CP-odd component's interference with the background is zero
when integrated over the other observables. The actual analysis of the data will
benefit from employing the full kinematics using the matrix-element approach. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[b]
\centerline{
\epsfig{figure=plots/jhugen-offshell.pdf,width=0.35\linewidth}
\epsfig{figure=plots/jhugen-offshell-vbf.pdf,width=0.35\linewidth}
}
\captionsetup{justification=centerlast}
\caption{
The four-lepton invariant mass distributions in gluon fusion (left) and in associated electroweak production with two jets 
(right) at the LHC with a 13\,TeV proton collision energy. The anomalous $HVV$ couplings are 
simulated with JHUGen+MCFM at LO in QCD, with the gluon fusion simulation settings and k factors matching those 
for SM in Fig.~\ref{fig:kinematics-ggH-offshell-1}. Three anomalous $HVV$ couplings are modeled 
with coupling values chosen to match the SM on-shell $gg\to H\to 2e2\mu$ cross section. 
% gluon fusion $H$ production process with decay to $2e2\mu$.
% The four-lepton invariant mass distributions in the gluon fusion (left) and electroweak (right) production 
% and $4\ell$ decay at the LHC with 13 TeV proton collision energy. The anomalous $HVV$ couplings are 
% simulated with the JHUGen+MCFM at LO in QCD, with the gluon fusion simulation matching that 
% in Fig.~\ref{fig:kinematics-ggH-offshell-1}. Three anomalous couplings are modeled, as indicated in the
% legend, with coupling values chosen to match the SM cross section in the \onshell\ gluon fusion process. 
Interference with the SM  $gg\to 4\ell$ (left) and electroweak (right) background is included. 
}
\label{fig:kinematics-offshell}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%


\subsection{\Offshell\ effects due to HVV anomalous couplings}
\label{offshell_hvv}

The \offshell\ production of the \Hboson also allows testing the anomalous $HVV$ couplings of the \Hboson to 
two electroweak bosons, $VV=WW, ZZ, Z\gamma, \gamma\gamma$. These couplings appear in the decay $H\to 4f$
in the gluon fusion process and in both production and decay in the electroweak process. The latter includes both VBF
and $VH$ production, and in all cases interference with the gluon fusion or electroweak background is included. 
Examples of such a simulation are shown in Fig.~\ref{fig:kinematics-offshell}. Three anomalous couplings are 
shown for illustration, $g_4^{ZZ}=g_4^{WW}$, $g_2^{ZZ}=g_2^{WW}$, and $\kappa_{1,2}^{ZZ}=\kappa_{1,2}^{WW}$, 
which involve interplay of either the \Hboson\ or the $Z$ ($W$) boson going off shell. The anomalous couplings 
of the \Hboson\ to the photon are not enhanced off-shell and are not shown here, but can be considered in analysis. 
Therefore, it is important to stress here that it is natural to use the physical Higgs basis in the EFT analysis 
of the \offshell\ region, since the behavior of the couplings involving the photon is drastically different. 

Examples of applications of the tools developed here, both simulation and MELA discriminants, can be found 
in Refs.~\cite{Sirunyan:2019twz,Cepeda:2019klc} where simultaneous analysis of the \Hboson width and 
the couplings is performed both with the current LHC data and in projection to the HL-LHC. For example, 
with 3000\,fb of data, a single LHC experiment is expected to constrain 
$\Gamma_{\rm tot}=4.1^{+1.1}_{-1.1}$\,MeV, as shown in Fig.~106 of Ref.~\cite{Cepeda:2019klc}. 
%(see Fig.106 on page 179)
With the current data sample from the LHC experiments, the \offshell\ region significantly improves
the anomalous coupling constraints, even with $\Gamma_{\rm tot}$ profiled~\cite{Sirunyan:2019twz}. 
This is evident from the enhancement observed in Fig.~\ref{fig:kinematics-offshell}. 
The expected gain is not as large at the HL-LHC, as Fig.~39 of Ref.~\cite{Cepeda:2019klc} shows,
%(see Fig.39 on page 88)
because with access to smaller couplings, the electroweak
VBF and $VH$ production in the \onshell\ region plays a more important role. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\centerline{
%\epsfig{figure=plots/jhugen-offshell-vbs.pdf,width=0.35\linewidth}
\epsfig{figure=plots/jhugen-offshell-vbs-g2.pdf,width=0.35\linewidth}
\epsfig{figure=plots/jhugen-offshell-vbs-L1.pdf,width=0.35\linewidth}
}
\captionsetup{justification=centerlast}
\caption{
The four-lepton invariant mass distributions in $4\ell$ associated electroweak production with two jets at the LHC 
with a 13 TeV proton collision energy. The anomalous $HVV$ couplings are simulated with JHUGen+MCFM. The black distributions 
show two SM scenarios: background only (dashed) and the full contribution including the \Hboson\ (solid). 
The colored curves show an additional non-zero anomalous contribution from either $g_2^{ZZ}$ (left) or $\kappa_{1,2}^{ZZ}$ (right) 
in the \Hboson\ production component (blue solid), in the background-only component (red dashed), and including all contributions (magenta solid).
% The four-lepton invariant mass distributions in the electroweak production and $4\ell$ decay at the LHC with a 13 TeV 
% proton collision energy. The anomalous $HVV$ couplings are simulated with the JHUGen+MCFM.
% The black distributions show two SM scenarios: background only (dashed) and the full contribution including the \Hboson\ (solid).
% The colored curves show an additional anomalous contribution with non-zero $g_2^{ZZ}$ (left) and $\kappa_{1,2}^{ZZ}$ (right):
% in the signal-only \Hboson\ production (blue solid), in the background-only production (red dashed), and with all contributions 
% (magenta solid). 
}
\label{fig:kinematics-cont-offshell}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%


\subsection{\Offshell\ effects due to gauge boson self-interactions}
\label{offshell_ew}

In Sections~\ref{offshell_hgg} and~\ref{offshell_hvv},
only modifications of the \Hboson couplings to either strong or weak gauge bosons are considered, 
and the background contributions in either $gg\to4f$ or continuum electroweak production are assumed to be SM-like. 
However, as discussed in Section~\ref{sect:cp_couplings}, there is an intricate interplay between gauge boson self-couplings 
and \Hboson\ gauge couplings.  Therefore, under the EFT relationship, the $HVV$ anomalous contribution would affect 
the triple and quartic gauge boson self-couplings according to Eqs.~(\ref{eq:d1}--\ref{eq:d6}). 
Figure~\ref{fig:kinematics-cont-offshell} shows examples of the $m_{4\ell}$ distributions with anomalous 
gauge boson self-interactions in the EFT framework. These examples show the CP-conserving anomalous 
$g_2^{ZZ}$ and $\kappa_{1,2}^{ZZ}$ couplings, which are modified in Eqs.~(\ref{eq:deltaMW}--\ref{eq:kappa2Zgamma}) 
for the \Hboson\ couplings and in Eqs.~(\ref{eq:d1}--\ref{eq:d6}) for the gauge boson self-couplings, 
keeping all the other anomalous couplings at zero. 
The size of the anomalous contribution is taken to be similar to the current constraints
on anomalous \Hboson\ couplings~\cite{Sirunyan:2019twz} from LHC measurements. 
It is evident from Fig.~\ref{fig:kinematics-cont-offshell} that the resonant and nonresonant contributions
are of similar size in electroweak production and that there is a sizable interference between the two. 
While current analyses of LHC data typically consider the \Hboson couplings and gauge boson self-interactions
separately~\cite{Sirunyan:2019twz}, the unified framework will allow future joint constraints. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

