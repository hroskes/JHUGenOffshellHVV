

Dear Dr. Heller,

Please find below our detailed answers to each point raised by the referee:


> The authors present some new features of the JHUGen+MELA framework
> focussing on Higgs production in different production channels
> considering both on-shell and off-shell production.

We thank the referee for careful reading of the manuscript and provide
detailed answers below. 


> My main comment is that whilst all the examples given are interesting,
> most physics cases have already been studied in the literature. I
> believe the purpose of this work is to bring everything together
> within one framework which also allows a statistical analysis. However
> it is not clear if this work is supposed to be serve as a manual to
> the code. If this is a public code that the authors what to provide to
> the community then this should be discussed and at least some
> instructions should be outlined of how a user should use it. Maybe an
> Appendix would be appropriate.

First of all, let us point out that the work reported in this paper has
been developed over the past six years and was available to the users 
through a public web page with a manual, which are referenced as Ref.[93]: 
https://spin.pha.jhu.edu

There are indeed many cases in the literature where the physics cases
have been studied with our program, and we have references [57-74] to 
stress this in the introduction. For example, let us point to a PRD paper 
[65] from five years ago where the authors of that paper created Fig.1 
using our off-shell simulation:
https://journals.aps.org/prd/abstract/10.1103/PhysRevD.92.072010

Therefore, while we agree with you that many physics cases have been
studied, many of those studies were enabled by our original framework which 
we made publicly available. With this publication, we provide a proper 
reference for this framework with a proper documentation of all
the features. Our choice was not to publish a paper with each incremental 
update of the framework, but instead to accumulate all relevant features 
in a complete framework and publish the description and original ideas 
at this time. Therefore, it is inevitable that during these six years 
some physics cases have been studied by the users of our program and
of other programs. 

Nonetheless, all examples which we present in this paper are original 
in its nature and have not been studied to the same detail in the literature. 
For example, In Section VI, we introduce the framework where simultaneous 
measurement of five HVV and two Hgg couplings is performed from simultaneous 
analysis of production and decay information using full simulation of the 
process. There is no such example in the literature to date, and we urge the 
LHC collaborations to proceed with such an analysis. The existing studies
(such as STXS for example) focus on either production or decay, separately, 
and correlation of these are ignored.  In addition to the loss of sensitivity
by only looking at a subset of the information, this is a serious omission
in such channels as H->ZZ and H->WW, where acceptance in decay affects
the experimental analysis of production.

Another example from Section VII: we evaluate the simultaneous effects of 
anomalous Higgs boson couplings and EW couplings on observable distributions
and point to their important effect when they are considered jointly. 
In Section IX: interference effects between a possible new resonance, 
continuum production, and off-shell Higgs boson, all with possible anomalous 
couplings, are investigated for the first time. 

Therefore, while we agree with the referee that the purpose of this work 
is to bring everything together within one framework, we would still like
to stress that all examples presented are essential and original. We would 
also like to leave technical details of how to use the code to the manual 
posted on the web page supporting this frameworks, and instead focus on 
discussing the features and use cases, which should enable future (and past)
advanced analysis of LHC data. 

To address the above comments explicitly, we have also made small modification 
to the title, which now reads: "New features in the JHU generator framework: 
constraining Higgs boson properties from on-shell and off-shell production."
With this title we would like to stress that it is not only a documentation of
new features of the generators (this part of the title has not changed), but
also discussion of the physics cases related to the on-shell and off-shell Higgs
boson production. 


> I also have the following comments/questions:
> 1) How is Higgs production in gluon fusion computed? As far as I see
> in the framework only LO production is possible. I see HNNLO in the
> plots 15-16 but the authors do not even have a citation to the
> corresponding paper. Can the authors clarify how they obtain their
> predictions, is a uniform k-factor from Hnnlo applied over all m4l?

The off-shell simulation of unweighted events in gluon fusion production,
which includes H boson signal, background processes, and their interference,
is performed at LO in QCD. This is stated in Section IV A. There are no programs, 
to our knowledge, that allow such simulation of unweighted events at higher 
orders in QCD. 

We indeed apply a uniform K factor calculated with HNNLO, as captions in Figs. 15 
and 16 state correctly. It was indeed an omission on our side that the HNNLO program 
was not referenced. In fact, we had this reference in the MC section IV A earlier, 
but then lost it in the last steps of the editing process. We now restore it as
"HNNLO~\cite{Catani:2007vq,Grazzini:2008tf,Grazzini:2013mca}"


> 2) Can the authors relate their couplings to the widely used kappa
> framework? Is there a clear correspondence?

By the "kappa framework" we understand the referee refers to modifiers of the SM-like 
couplings. This means not allowing for new tensor structures and simply scaling the
strength of the Higgs boson to SM particles. This approach is naturally included in 
our framework and in any EFT interpretation in general. This simply means that all 
anomalous couplings are set to zero, and the SM couplings are allowed to scale. 
For example, if we take Eq.(1) in our paper, it means that g_1^ZZ and g_1^WW are allowed
to float and all other couplings are zero. So, in this case kappa_V = g_1/2, where 2 is
the SM expectation, as written just below Eq.(1). In Eq.(2) this means that kappa_f 
is allowed to float (it is literally the kappa_f in the kappa framework), while ~kappa_f=0.
We believe this should be transparent from the notation used, so we expect no modification 
to our text. 


> 3) Is there any well-motivated BSM scenario which would lead to k_1
> and k_2 for the Ws being different?

We understand the referee refers to Eq.(1) where we allow k_1^WW and k_2^WW
to be different. We do not suggest that there is a motivated BSM theory which
would lead to a difference. In fact, when we come to Eqs. (7) and (8), these 
are required to be equal, due to charge symmetry spelled out below these 
equations. However, we introduce Eq.(1) and setup the generator framework code
in the most general terms allowing all possible Lorentz-invariant structures. 
Reducing the number of these structures by setting certain contributions 
to zero is easy to achieve for the users of our framework. We believe no 
modification to the paper is required because we already discuss k_1^WW and 
k_2^WW in the paper. 


> 4) The authors talk about different EFT bases and how to relate
> different bases coefficients. Rosetta is a tool which can be used for
> that.

We now add a reference to Rosetta~\cite{Falkowski:2015wza} in the Appendix 
and refer to Eq.(14) in that paper, which quotes the relationship between 
the CP-even operators. We also expand our discussion of the basis translation. 
However, since there is no explicit relationship of the CP-odd operators 
in this reference, our formulas in the Appendix for those operators are still 
important to present. 


> 5) Eq. 43 ignores any interference terms. I think it's an
> over-simplified and potentially misleading picture. Could the authors
> add some word of caution?

There must be some confusion: Eq.(43) is nothing more than inversion of Eq.(42), 
which is a mathematical statement. Nothing is ignored in this mathematical 
expression, it is exact. 

The referee probably would like to say that Eq.(42) does not introduce interference
terms in the denominator. This is correct, as it was stated explicitly just below 
Eq.(42): "In the denominator of Eq. (42), the sum runs over all couplings contributing 
to the i -> H or H -> f process. By convention, the interference contributions are not 
included for ease of interpretation." Therefore, Eq.(42) is simply a convention to 
define f_an, which has been established since 2012, when the very first measurement 
of f_a3 was performed, and many other measurements of f_an followed. References to
the definition of f_an established by both ATLAS and CMS follow in the next sentence 
with explicit Refs. [64, 67, 92]. 

In fact, we could make an even stronger statement and say that including interference 
in Eq.(42) would be wrong, as this could introduce degeneracy and there would not
be a one-to-one correspondence of anomalous coupling ratios and f_ai. 


> 6) In the width computation of H->4f are interference effects between
> WW and ZZ taken into account?

It is not fully clear if the referee points to the width appearing in Eq.(34)
or to a more general framework on the JHU generator. However, in both cases 
the answer is no for the reason that this is a negligible effect. This is because
in decay the final states have very different phase-space population. 

In the JHU generator framework in general, which is used to generate un-weighted events for
LHC analysis, we have separate processes for H->WW->4f and H->ZZ->4f. First of all, only 
some final states could potentially interfere, for example 4f=2l2nu could interfere, 
but 4f=4l cannot interfere. Second, for the final states where interference is possible, 
it is very small because phase-space overlap of these final states is very small. We consider
the off-shell production in particular, where both Z or W are on-shell. Therefore it is
extremely unlikely for leptons from WW->2l2nu to match both m_ll and m_nunu to the Z boson
mass to appear in the ZZ phase space. Moreover, all examples in our paper are with H->4l, 
where interference is not even possible. 

As for the width calculation in Eq. (34), from this sum and supporting Table 1, it should 
be clear that we consider H->WW->4f and H->ZZ->4f separately, which implies no interference 
included. For the same reasons mentioned above, interference is a negligible effect, especially 
considering that H->ZZ->4f contributes only 2.6% to the total width, and only a very small 
fraction of its phase space and only for some final states would interfere with H->WW->4f. 
In the end, we use Eq. (34) only for illustration of how the results could be included in
the "global fits," while we do not really perform the global fits. The goal of the JHU
generator framework is to facilitate experimental analysis of individual channels with 
LHC data, and the width parameterization does not matter for this purpose. 


> 7) Do the authors consider the gluon fusion + 2jets contamination to VBF?

Yes, of course. We state it in Section VI that all processes, including anomalous
couplings, are generated with JHUGen. Then we discuss detailed categorization of events, 
which targets all major production mechanisms, and obviously cross-feed of different 
processes between different categories is included. In that regard, ggH+2jets is 
background for VBF and VBF is background for ggH+2jets. To make this even more 
transparent, we adjust one of the introductory paragraphs in Section VI and include 
the following sentence in particular: 

"... All production modes of the H boson are included in this study and are generated 
with the JHU generator as discussed in Section IV..."


> 8) gg>ZH has been studied in the context of also top EW couplings and
> found to be rather sensitive to them (1601.08193,1603.05304). Maybe
> the authors can comment on this and the potential impact on what one
> can learn about Higgs couplings from this process.

> 15) page 27: section C: There is also a very interesting interplay
> with top couplings in off-shell Higgs (1406.6338, 1608.00977)

We thank the referee for mentioning these works which relate to our study. 
Since point 8) and point 15) are very similar, we address them together here. 

On point 15), we assume that the referee is actually commenting on Sect.B instead of Sect.C. 
The works 1406.6338 and 1608.00977 deal with off-shell Higgs production in gg->H->4l, 
which we only discuss in Sect. B. 

It is certainly interesting to study top quark couplings in H boson production processes. 
Therefore, we add a few sentences in Sect. B and explain our strategy:

>>> 
We point out that the authors of Refs.~\cite{Azatov:2014jga,Azatov:2016xik} 
also find good sensitivity for constraining top quark electroweak couplings in the $gg \to 4l$ process.
Similarly, gluon fusion in $ZH$ production is sensitive to the 
same top quark couplings and can be used to constrain them~\cite{Bylund:2016phk,Englert:2016hvy}. 
In this work, however, we separate anomalous H boson couplings from the rest of the SM, if this is 
possible in a consistent way. 
For the above cases this is certainly the case because top quark electroweak couplings can also 
be probed in e.g. $pp \to t\bar{t}Z$, which is independent of the Higgs sector. 
Moreover, there are no EFT relations between electroweak top quark couplings and \Hboson couplings. 
However, we point out that  this is not the case for gauge boson self-interactions and the \Hboson couplings,
if EFT relations Eqs.~\ref{} are applied in the continuum electroweak production. 
In this case we can account for these relations as discussed in the following section. 
<<<


> 9) The authors claim that the Higgs basis is ideal for the off-shell
> process. How about when combining with other Higgs processes in global
> fits? Do the authors foresee any problems?

We generally do not expect any problems for the following reasons. First of all,
most of the published Higgs boson results have used the Higgs basis already. 
Just as the latest example, Ref.[70,71] present both on-shell and off-shell 
and both H->4l and H->TauTau measurements by CMS in the Higgs basis, and their 
combination is performed naturally. Second, even if other measurements are 
performed in a different basis, we stress it in Section V and in Appendix and 
expand this point even more now (see above) that there is essentially a one-to-one 
correspondence between the Higgs and Warsaw bases (in this example, but this 
applies to other bases as well). Therefore, the measurements should be performed 
in the appropriate basis, which makes the measurements easy to perform and results
easy to present, and their interpretation can be trivially transformed in 
the global fits for example. This has already been discussed in Section V. 


> 10) Are the plots in fig. 6 normalised to 1? Can the authors add the
> y-axis label?

We now add a statement in the caption: "All distributions are normalized to unit area."


> 11) I was a bit confused as to why the authors show an example where
> machine learning is actually degrading the performance. Do the authors
> have an example to demonstrate that ML can improve the discrimination?

Machine learning was not our main target of the paper, and therefore we did not
set the goal to prove the case where machine learning outperforms other techniques. 
Instead, our goal was to show that in a controlled environment where everything 
else is equal, the matrix element approach and machine learning are equivalent. 
We believe that we have effectively illustrated this with two examples and from 
general principles. In one example the equivalence is exact, and in the other 
more complicated case we believe the shortcoming of the training process did not 
allow us to show the full equivalence, but it is very close. The referee suggests 
to examine an example where ML outperforms the matrix element approach. However, 
this would be an example where information used by ML goes beyond that used by 
the matrix elements. This would not be a controlled environment of two approaches 
with the same input. More information would lead to better performance, if used 
correctly. 


> 12) Just a comment: The authors are rather critical of STXS. It is of
> course expected that one will have a better sensitivity when using
> optimal discriminants designed with particular couplings in mind.
> Therefore I am not sure the comparison is a fair one. It is well-known
> that STXS is a "theory-experiment" compromise which continues to
> evolve to accommodate more effects and better BSM sensitivity.

What we point in our paper is that no scientific compromise has to be made when analysis
of LHC data is performed. The experimental analysis could be performed in an optimal, 
unbiased, and honest way. The results can be passed to the theory interpretation and
used as easily as the STXS output. 

There are two problems with the STXS compromise: (1) its less than optimal performance
due to not utilizing observables optimal for the measurements, and (2) the bias in the
procedure which uses only SM kinematics of simulated events to extract differential 
distributions from data, and then makes interpretation of these distributions for BSM
models (non-SM tensor structures of interactions) which have non-SM kinematics. 

The first fact (1) is generally known, but the exact quantitative effect in not known.  
In our paper, we present a quantitive estimate of this effect for the first time. 
In fact, we have received comments from people that they were surprised that the effect 
was so large. Therefore, documenting this effect is important. 

The second fact (2) is generally not known and is widely overlooked. However, it is even
more important than the first one. While effect (1) could lead to worse precision, the
second effect (2) would lead to wrong (biased) results. We stress this in the introduction 
to Section V, just before subsection A with the following words: "... the STXS measurements 
are based on the analysis of SM-like kinematics. The measurement strategy may not be 
appropriate for interpretations appearing with new tensor structures or new virtual 
particles (such as gamma* in place of Z*) unless a full detector simulation of such 
effects is performed." We also discuss this a bit more in Section VI. 


> 13) Page 20: Can the authors summarise by saying how many free
> parameters they eventually have in their analysis?

There are a total of seven free parameters describing HVV and Hgg coupling in our fit. 
This was implied in the sentence "The constraints on each parameter are shown with the 
other parameters describing the $HVV$ and $Hgg$ couplings profiled." However, we have 
now extended this sentence and made it explicit. 


> 14) Page 23: "However, we point out that unless the particles in the
> loop are light, their mass does not significantly affect the
> kinematics of the H boson and associated jets. " I don't think this is
> true. An additional heavy particle in the loop will give a very
> different Higgs pT distribution compared to the SM one.

We do state it in the following sentence that pT could be affected slightly, but this is not 
a relevant effect for our analysis. We now make a stronger and clearer statement in the same
place in the paper as follows: "The main effect is on the H boson's transverse 
momentum~\cite{deFlorian:2016spz}, where heavy particles in the loop may enhance the tail 
of the distribution at $p_T>200$\,GeV, but not affect much the bulk of the distribution 
at $p_T<200$\,GeV, relevant for our study." This is a known effect in the literature and 
we believe we do not need to provide more evidence in the paper beyond the above reference. 
However, to re-assure the referee, we attach a figure referee_comments_support.pdf 
which shows that in the case we take the top mass to be very large, the pT distribution below
200 GeV is not affected, and above 200 GeV the fraction of events is extremely small. 
The effect is much more important if one considers light particles in the loop instead, 
as can be seen on the plots, but we exclude this possibility in the studies. These plots have 
been created with the HNNLO+MCFM framework used for other calculations referenced in our paper. 


> 16) page 28: Is there a physical explanation why g2^VV and g4^VV drop
> out of gg>ZH?

We looked into it but we have not found a satisfactory explanation. 
The feature appears non-trivial to us because of the interplay of the 
closed massive fermion loop with VVA structure and the HVV vertex, which 
are separated by a massive Z boson propagator. This certainly deserves 
a closer look in future work. 

To the best of our knowledge this feature has not been explained elsewhere 
in the literature. In 1602.04305 the observation that g4^VV drop outs 
has been made, but no physical explanation is given.


> 17) page 28: "As an example, it is possible to enhance the
> gg-initiated process fraction from 7% to 22% with a modest 20%
> reduction in its cross section." Can the authors explain this better?
> It is not clear to me what they mean.

We have rephrased this sentence as follows: "As an example, in a restricted 
range of the D_ggZH observable which keeps 80\% of the $gg$-initiated process,  
the fraction of this process is enhanced from 7\% to 22\%." We present numerical 
values which could be compared to Ref.[120], referenced in the next sentence. 
This illustrates the power of this approach compared to simple cuts on observables. 


> 18) page 29: "it might come from a modication of the H boson width"
> The width doesn't modify off-shell kinematics. What do the authors
> mean?

We were indeed sloppy in writing this statement. What we meant is that given the on-shell H boson
peak established at 125 GeV, the relative size of the off-shell production would be affected by 
the H boson width. However, instead of clarifying this aspect, we simply removed this part of the
sentence. The general idea is passed with the remaining part. 


> 19) fig. 21 why does the first plot not have the uncertainty bands? 

The uncertainty bands were not visible because of the log scale used. However, we have now
remade this plot with a linear scale and the bands are now visible. We have also taken this 
chance to update the plots in Fig.21 to synchronize their style. The content of the plots has
not changed. 


> 20) Figs 22 and 23 are rather busy. Can the authors zoom in the resonance region?

We have zoomed into the resonance region in both figures. 

In conclusion, let us also say that we have extended the list of references.  In addition to the 
papers mentioned in the comments above, we received several requests for references from external 
readers of the arXiv pre-print, which arrived late for this submission. Therefore, some of the 
references mentioned above might have changed their number in the latest version of the paper. 
However, none of these changed any content, conclusions, or results of the paper. We have refined 
Figs.21-23, per request from the referee, and Fig.18 where we found that one histogram required 
higher statistics of the MC sample to be more precise.

